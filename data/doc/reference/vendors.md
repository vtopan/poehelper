# Vendor Recipes

## Undo Recipes

- uncomplete white/yellow/red map:
    - 1 {scour} + 3 [Apprentice Cartographer's Sextant]
        => [Apprentice Cartographer's Seal]
    - 1 {scour} + 3 [Journeyman Cartographer's Sextant]
        => [Journeyman Cartographer's Seal]
    - 1 {scour} + 3 [Master Cartographer's Sextant]
        => [Master Cartographer's Seal]
- unshape map: 20 {chisel} + 5 {regret}
- level down character:
    1 scour + 1 ID scroll = [Book of Regression]
- change bandit quest reward ([Book of Reform]):
    - to Oak: 20 {regret} + 1 [Amber Amulet]
    - to Kraityn: 20 {regret} + 1 [Jade Amulet]
    - to Alira: 20 {regret} + 1 [Lapis Amulet]
    - to Eramir: 20 {regret} + 1 [Onyx Amulet]
- decrease gem level:
    - by 1: gem + 1 {scour}
    - to 1: gem + 1 {regret}


## Crafting

- White Boots + [Quicksilver Flask] + {aug}
    => Magic Boots with 10% movement speed
- Magic Boots (X% MS) + [Quicksilver Flask] + {aug}
    => Magic Boots with X+5% movement speed
- todo


## Unique Items

- [Primordial Might] + P. Harmony + P. Eminence
    => [The Anima Stone]
- [Grelwood Shank] + [Beltimber Blade] + {fusing}
    => [Arborix]
- todo


## Todo ...


## Reference

- https://pathofexile.gamepedia.com/Vendor_recipe_system
