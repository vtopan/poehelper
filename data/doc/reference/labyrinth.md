# Labyrinth


## Trials (Normal Lab)

|&#x25C9; Prison L1 (A1)          |&#x25C9; Crematorium (A3)     |
|&#x25C9; Crypt L1 (A2)           |&#x25C9; Catacombs (A3)       |
|&#x25C9; Chamber of Sins L2 (A2) |&#x25C9; Imperial Gardens (A3)| 


## Trials (Cruel Lab)

&#x25C9; The Prison (A6)
&#x25C9; The Crypt (A7)
&#x25C9; Chamber of Sins (A7)


## Trials (Merciless Lab)

&#x25C9; The Bath House (A8)
&#x25C9; The Tunnel (A9)
&#x25C9; The Ossuary (A10)


## Trials (Maps)

|&#x25C9; Piercing Truth      |&#x25C9; Burning Rage   |
|&#x25C9; Swirling Fear       |&#x25C9; Lingering Pain |
|&#x25C9; Crippling Grief     |&#x25C9; Stinging Doubt |
