# Prophecies

## Normal
* map mods
- [Bountiful Traps] => 6 strongboxes
- [The Dream Trial] => [Trial of Ascendancy]
- [The Emperor's Trove] => extra gold key in [The Lord's Labyrinth]
- [The Fortune Teller's Collection] => [Fortune Teller's Chest] (div cards)
- [Twice Enchanted] => extra enchantment in [The Lord's Labyrinth]
* crafting
- [Fated Connections] => 6L an item ($$$)
- [The Jeweller's Touch] => 6S/5L an item
- [Trash to Treasure] => make item unique ($$$)
* item drops
- [The Lost Maps] => ~6 T1 maps
- [The Dreamer's Dream] => rare map from boss
- [The Mysterious Gift] => 5:1 uniques
- [Erasmus' Gift] => 20% quality gem
- [Overflowing Riches] => 10 currency items
- [The God of Misfortune] => 3 [Orb of Fusing]
- [Reforged Bonds] => 5 [Orb of Fusing]
* uniques
- [A Prodigious Hand] => [Maligaro's Virtuosity] ($$)
- [Blood in the Eyes] => [The Bringer of Rain]
- [Cleanser of Sins] => [Tabula Rasa]
- [Fire, Wood and Stone] => [Marohi Erqi]
- [Flesh of the Beast] => [Belly of the Beast]
- [Lost in the Pages] => [Astramentis]
- [The Flayed Man] => [Bino's Kitchen Knife]
- [The Last Watch] => [Drillneck]
- [The Walking Mountain] => [Clayshaper]
- [The Watcher's Watcher] => [Eye of Chayula]
- [Weeping Death] => [Rainbowstride]
- [Wind and Thunder] => [Windripper]
- [The Soulless Beast] => [Atziri's Foible]
- [The Queen's Vaults] => [Vaults of Atziri]
- [The Nest] => [Rat's Nest]
* fated uniques
- [The Queen's Sacrifice] + [Atziri's Mirror] => [Atziri's Reflection] ($$$)
- [Battle Hardened] + [Iron Heart] => [The Iron Fortress]
- [Fire and Brimstone] + [Blackgleam] => [The Signal Fire]
* no reward
- [Plague of Frogs] => increased currency / div card drops
- [Monstrous Treasure] => no monsters, 36 strongboxes
- [Deadly Twins] => double map bosses
- [The Invader] => [Invasion] boss
- [Mysterious Invaders] => [Beyond] demon
- [The Cursed Choir] => [Sea Witches]

## Chains
- the first N-1 in each chain provide no reward
- [Ancient Rivalries IV] => [Hinekora's Sight]
- [The Ambitious Bandit III] => [The Ascetic]
- [The Pale Court] fragments:
* [The Feral Lord V] => [Yriel's Key]
* [Unbearable Whispers V] => [Inya's Key]
* [The Plaguemaw V] => [Eber's Key]
* [The Unbreathing Queen V] => [Volkuur's Key]
- [The Warmongers IV] => warband uniques
- [Deadly Rivalry V] => [Kintsugi]
- [Day of Sacrifice IV] => [Sacrifice at Midnight]
- [Thaumaturgical History IV] => [Ascent From Flesh]
- [Beyond Sight IV] => nothing (kill [Herald of Bameth])

