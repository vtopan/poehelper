# Pantheon

## Brine King

You cannot be Stunned if you've been Stunned or Blocked a Stunning Hit in the past 2 seconds

[Glace] @ [Beach Map]
30% increased Stun and Block Recovery
[Champion of the Hollows] @ [Crystal Ore Map]
You cannot be Frozen if you've been Frozen Recently
[Fragment of Winter] @ [Waterways Map]
50% reduced Effect of Chill on you


## Lunaris

1% additional Physical Damage Reduction for each nearby Enemy, up to 8%
1% increased Movement Speed for each nearby Enemy, up to 8%

[Ancient Architect] @ [Pier Map]
10% chance to avoid Projectiles
[Shock and Horror] @ [Mineral Pools Map]
5% chance to Dodge Attack and Spell Hits if you've been Hit Recently
[The Hallowed Husk] @ [Palace Map]
Avoid Projectiles that have Chained


## Solaris

6% additional Physical Damage Reduction while there is only one nearby Enemy
20% chance to take 50% less Area Damage from Hits

[Vision of Justice] @ [Orchard Map]
8% reduced Elemental Damage taken if you haven't been Hit Recently
[Eater of Souls] @ [Core Map]
Take no Extra Damage from Critical Strikes if you have taken a Critical Strike Recently
[Kitava, The Destroyer] @ [Lava Lake Map]
50% chance to avoid Ailments from Critical Strikes


## Arakaali

5% reduced Damage taken from Damage Over Time
10% chance to Avoid Lightning Damage when Hit

[Queen of the Great Tangle] @ [Jungle Valley Map]
50% increased Recovery of Life and Energy Shield if you've stopped taking Damage Over Time Recently
[Shavronne the Sickening] @ [Cells Map]
30% reduced Effect of Shock on you
30% reduced Shock Duration on You
[Thraxia] @ [Spider Lair Map]
+25% Chaos Resistance against Damage Over Time


## Garukhan

@ [The High Gardens] (A8)
5% chance to Evade Attacks if you've taken a Savage Hit recently

[Stalker of the Endless Dunes] @ [Dig Map]
6% increased Movement Speed if you haven't been Hit Recently


## Yugul

@ [The Quarry] (A9)
You and your Minions take 25% reduced Reflected Damage
50% chance to Reflect Enemy Chills and Freezes

[Varhesh] @ [Terrace Map]
5% reduced Cold Damage taken if you've been Hit Recently


## Abberath

@ [Prisoner's Gate] (A6)
5% reduced Fire Damage taken while moving
Unaffected by Burning Ground

[Mephod] @ [Summit Map]
50% reduced Ignite Duration on you
10% increased Movement Speed while on Burning Ground


## Tukohama

@ [The Karui Fortress] (A6)
While stationary, gain 2% additional Physical Damage Reduction every second, up to a maximum of 8%

[Tahsin] @ [Siege Map]
While stationary, gain 0.5% of Life Regenerated per second every second, up to a maximum of 2%


## Gruthkul

@ [The Dread Thicket] (A7)
1% additional Physical Damage Reduction for each Hit you've taken Recently up to a maximum of 5%

[Erebix] @ [Cemetery Map]
Enemies that have Hit you with an Attack Recently have 8% reduced Attack Speed


## Ralakesh

@ [The Ashen Fields] (A7)
25% reduced Physical Damage over Time Damage taken while moving
25% chance to avoid Bleeding

[Drek] @ [Fields Map]
Cannot be Blinded
You cannot be Maimed


## Ryslatha

@ [The Wetlands] (A6)
Life Flasks gain 3 Charges every 3 seconds if you haven't used a Life Flask Recently

[Gorulis] @ [Infested Valley Map]
60% increased Life Recovery from Flasks used when on Low Life


## Shakari

@ [The Sand Pit] ([The Oasis], A9)
5% reduced Chaos Damage taken
25% reduced Chaos Damage over Time taken while on Caustic Ground

[Terror of the Infinite Drifts] @ [Desert Spring Map]
50% less Duration of Poisons on You
You cannot be Poisoned while there are at least 5 Poisons on you
