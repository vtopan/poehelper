# Heist

## Rogues

- **Karst**: Lockpicking L5, Perception L3, Agility L2
	- 15-40% less raising of Alert Level from opening Reward Chests
- **Niles**: Counter-Thaumaturgy L5, Deception L4
	- Can unlock 1 Magical Chest after Lockdown per Wing
	- Discovers a Blueprint Reveal upon completion of a Heist Contract Mission
- **Huck**: Lockpicking L3, Bruteforce L3, Demolition L3, Engineering L3
	- Is a powerful Combatant and provides strong Buffs to Allies
- **Tibbs**: Bruteforce L5, Demolition L4
	- Can unlock 1 Mechanical Chest after Lockdown per Wing
- **Nenet**: Perception L5, Counter-Thaumaturgy L4
	- Can Scout nearby Enemy Patrols and Elite Patrols during Heists
	- 5-20% less raising of Alert Level
- **Vinderi**: Demolition L5, Trap Disarmament L5, Engineering L2
	- 10-30?% more raising of Alert Level
	- Heist Chests have a 5-20?% chance to Duplicate their contents
- **Tullina**: Agility L5, Lockpicking L3, Trap Disarmament L2
	- 6-26?% chance to find additional Heist Targets from Secret Reward Rooms
	- 4-14% less raising of Alert Level
- **Gianna**: Deception L5, Counter-Thaumaturgy L3, Perception L2
	- Discovers a Blueprint Reveal upon completion of a Heist Contract Mission
	- 5-25?% reduced Blueprint Revealing Cost
- **Isla**: Engineering L5, Trap Disarmament L4
	- 30-50?% increased time before Lockdown

## Rewards

- **Agility**: {Currency}, {Armour}, {Harbinger}, {Essences}, {Fossils}
  - Karst L2, Tullina L5
- **Bruteforce**: {Uniques}, {Weapons}
  - Huck L3, Tibbs L5
- **Counter-Thaumaturgy**: {Gems}, {Trinkets}
  - Niles L5, Nenet L4, Gianna L3
- **Deception**: {Armour}, {Divination Cards}
  - Niles L4, Gianna L5
- **Demolition**: {Generic}, {Blight}, {Metamorph}
  - Huck L3, Tibbs L4, Vinderi L5
- **Engineering**: {Uniques}, {Maps}, {Armour}
  - Huck L3, Vinderi L2, Isla L5
- **Lockpicking**: {Currency}, {Trinkets}, {Fragments}
  - Karst L5, Huck L3, Tullina L3
- **Perception**: {Trinkets}, {Prophecies}, {Divination Cards}
  - Karst L3, Nenet L3, Gianna L2
- **Trap Disarmament**: {Armour}, {Weapons}, {Abyss}, {Breach}, {Talismans}, {Legion}
  - Vinderi L5, Tullina L2, Isla L4

## Unlocking Rogues
- Isla - always unlocked
- Tibbs => Tullina => Nenet
- Karst => Huck => Niles => Vinderi => Gianna

## Strategy

- good rewards:
	- Dem: Blight, Metamorph
	- TD: Breach, Legion
	- A: Currency, Harbinger, Fossils
	- L: Currency, Trinkets, Fragments
	- P: Trinkets, Prophecies Div Cards
	- E: Maps
- Grand Heist:
	- +2 reward chests:
		- Tibbs: B <= 5 or Dem <= 4, and
		- Niles: CT <= 5 or Dec <= 4
	- less raising:
		- Karst (40% for reward chests): L <= 5 or P <= 3 or A <= 2
		- Nenet (20%): P <= 5 or CT <= 4
	=> Dem + CT + P best overlap

	