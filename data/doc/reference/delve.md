# Delve

## Levels / Depth
* monster levels:
    - 60 @ depth 40
    - 75 @ depth 117-129
    - 80 @ depth 191-204
    - 83 @ depth 239+
* increased monster life/damage @ 300+
* Resonators:
	- Potent (2) / Powerful (3) @ 34+
	- Prime (4) @ 68+

## Mines
- depth: 1-52, more @ 1-30
Aetheric: +caster, -attack
Metallic: +lightning, -phys
Pristine: +life, -ar/es/ev
Serrated: +attack, -caster

## Fungal Caverns
- depth: 5+, more @ 20+
+Dense: +ar/es/ev, -life
+Gilded: +vendor price (behind walls)
Aberrant: +chaos, -lightning
Corroded: +poison/bleed, -elemental
Perfect: +quality

## Petrified Forrest
- depth: 5+, more @ 20+
Bound: +minion/aura
Corroded: +poison/bleed, -elemental
Jagged: +phys, -chaos
Sanctified: +lucky rolls/high tiers (behind walls)

## Abyssal Depths
- depth: 11+, more @ 25+
+Gilded: +vendor price
Aberrant: +chaos, -lightning
Bound: +minion/aura
Lucent: +mana, -speed (behind walls)

## Frozen Hollow
- depth: 16+, more @ 30+
Frigid: +cold, -fire
Prismatic: +elemental, -poison/bleed
Serrated: +attack, -caster
Sanctified: +lucky rolls/high tiers (behind walls)

## Magma Fissure
- depth: 21+, more @ 40+
Prismatic: +elemental, -poison/bleed
Pristine: +life, -ar/es/ev
Scorched: +fire, -cold
Enchanted: +lab enchant (behind walls)
Encrusted: +sockets/white (behind walls)

## Sulphur Vents
- depth: 46+, more @ 75+
Aetheric: +caster, -attack
Metallic: +lightning, -phys
Perfect: +quality
Encrusted: +sockets/white (behind walls)

## Vaal Outpost
- depth: 23+, more @ 52+
- [Ahuatotli, the Blind] @ 83+

## Abyssal City
- depth: 52+, more @ 52+
- [Kurgal, the Blackblooded] @ 150+

## Primeval Ruins
- depth: 220+, more @ 600+
- [Aul, the Crystal King] @ 250+

## "Contains Fossils" nodes
- common @ 600+
++Bloodstained: +vaal mod
++Fractured: +mirror
++Hollow: +abyssal socket
+Faceted: +gem level
+Glyphic: +corrupt essence
+Tangled: +any fossil
Shuddering: +speed, -mana

## Reference

- https://pathofexile.gamepedia.com/Delve
- http://poedb.tw/us/DelveBiomes
- fossil crafting: https://poedb.tw/us/mod.php?cn=Claw
