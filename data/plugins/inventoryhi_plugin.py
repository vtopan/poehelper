"""
PoEHelper inventory highlight plugin.

Author: Vlad Topan (vtopan/gmail)
"""

try:
    from cgi import escape
except ImportError:
    from html import escape

from poehelper.poeh_plugins import PoehPlugin



class InventoryHighlightPlugin(PoehPlugin):
    CFG = {
        'expand_on_wtb': True,
        }


    def init(self):
        """
        Initialize the plugin.
        """
        self.api.create_plugin_panel('Inventory Hi.',
            """
            L<Name>
            [|LLLLLLLLLLLL]<Matrix>
            """)
        self.panel.set_item_text('Name', 'Nothing to highlight (wait for a WTB msg)')
        contents = [''] * 12
        for i in range(12):
            self.panel.add_grid_cell(i, *contents)
            self.panel.set_grid_item(i, 0, '')
        self.highlighted = []    
            
            
    def wtb_pm_handler(self, msg, **kwargs):        
        """
        Handle WTB message events created by the trade plugin (issued on buy requests).
        """
        if msg.direction == 'From' and msg.x is not None:
            item, stash = escape(msg.item), escape(msg.stash) 
            self.panel.set_item_text('Name', f'WTB <font color=red><b>{item}</b></font><br>Stash: <b>{stash}</b> @ {msg.x},{msg.y}')
            for x, y in self.highlighted:
                self.panel.grid_item(y, x).setStyleSheet('* {background-color: #000;}')
            x, y = msg.x - 1, msg.y - 1
            self.highlighted = [(x, y)]
            self.panel.grid_item(y, x).setStyleSheet('* {background-color: #F00;}')
            if self.cfg['expand_on_wtb'] and (self.api.in_town or self.api.in_hideout):
                self.api.temporarily_expand()
                self.api.show_plugin_panel()
        