"""
PoEHelper multiple item worth estimation plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import re
import time

from PySide2 import QtGui, QtCore

from poehelper.poeh_plugins import PoehPlugin



class ItemWorthPlugin(PoehPlugin):
    CFG = {
        'cols': 3,
        }


    def init(self):
        self.api.create_plugin_panel('Item Worth',
            f"""
            T<Name>
            B<Start> B<Calculate> B<Save as image>
            [{'L' * self.cfg['cols']}]
            L<Uniques>
            L<Total>
            """)
        self.items = []
        self.pn_prices = {}
        self.chaos_img = self.api.get_image_string('currency', 'Chaos Orb')
        self.exa_img = self.api.get_image_string('currency', 'Exalted Orb')
        self.panel.set_item_text('Name', 'Item worth calculator - Press Start,'
            'hover items in game & press Ctrl+C, then press Calculate')
        self.panel.set_button_click('Start', self.start)
        self.panel.set_button_click('Calculate', self.calculate)
        self.panel.set_button_click('Save as image', self.save_as_image)
        # fixme: add description field to pluginpanel or fix QLabel overflow
        name = self.panel.get_item('Name')
        name.setFixedHeight(64)
        name.setReadOnly(True)
        self.running = 0


    def start(self):
        """
        Start watching (prevent other plugins from accessing the Ctrl+C hook).
        """
        self.running = 1
        self.items = []
        self.api.log('Starting item worth watch; click game window first!')
        

    def calculate(self):
        """
        Stop watching & compute the totals.
        """
        pn = self.api.poeninja
        items, cats = {}, set(['Currency'])
        # self.panel.clear_grid()
        if not self.items:
            self.api.log('No items added! Was the game window focused?')
            return
        self.api.log(f'Evaluating {len(self.items)} items...')
        try:
            for item in self.items:
                if set(item['types']) & {'currency', 'fossil', 'divinationcard', 'fragment',
                        'prophecy'} or item.get('rarity') == 'unique':
                    cat = pn.category(item['types'], rarity=item.get('rarity'))
                    if cat:
                        name = item.get('name', item['types'][1])
                        count = item.get('count', 1)
                        item['sort_key'] = (item['types'][0],
                                item['types'][min(2, len(item['types']) - 1)], item['types'][1])
                        if name not in items:
                            items[name] = [name, item, count, cat]
                        else:
                            items[name][2] += count
                        if cat not in self.pn_prices:
                            cats.add(cat)
                    else:
                        self.api.err('Can\'t get poe.ninja category for '
                                f'{item.get("name", item["types"][1])}!')
                else:
                    self.api.log(f'Ignoring {item.get("name", item["types"][1])}')
            if not items:
                return
            for cat in cats:
                self.pn_prices[cat] = pn.brief_result(pn.query_prices(cat,
                        self.cfg['game']['league']))
            total = 0
            results, uniques = [], []
            items = sorted(items.values(), key=lambda x:x[1]['sort_key'])
            for name, item, count, cat in items:
                if name == 'Chaos Orb':
                    price = 1
                else:
                    price = self.pn_prices[cat].get(name, None)
                if not price:
                    self.api.log(f'Failed getting price for {name}!')
                else:
                    price = price * count
                    total += price
                    price_str = re.sub(r'\.?0+$', '', f'{price:.3f}')
                    if item['types'][0] in ('fragment', 'currency'):
                        icon = self.api.get_image_string(item['types'][0], name)
                        if icon:
                            name = icon
                    else:
                        name = ' x ' + name
                    s = f'{count}{name} = <b>{price_str}</b>{self.chaos_img}'
                    if 'unique' == item.get('rarity') or item['types'][0] == 'prophecy':
                        uniques.append(s)
                    else:
                        results.append(s)
            pos, cols = 0, self.cfg['cols']
            for pos, s in enumerate(results):
                if pos % cols == 0:
                    line = pos // cols
                    print(self.panel.grid().rowCount(), line + 1)
                    if self.panel.grid().rowCount() <= line + 2:
                        self.panel.add_grid_cell()
                self.panel.set_grid_item(pos // cols, pos % cols, s)
            self.panel.set_item_text('Uniques', '<br>'.join(uniques))
            exa_price = self.pn_prices['Currency']['Exalted Orb']
            exa = ''
            if total > 0.5 * exa_price:
                exa = f' / {total / exa_price:.1f}{self.exa_img}'
            self.panel.set_item_text('Total', f'Total: <b>{int(total)}</b>{self.chaos_img}{exa}')
            self.panel.autosize()
        except Exception as exc:
            self.api.err(f'Failed: {self.api.brief_exc(exc)}')
        self.running = 0


    def pre_ingame_ctrlc_handler(self, info, text, **kwargs):
        if self.running:
            self.items.append(info)
            return True


    def save_as_image(self):
        """
        Save totals as image.
        """
        fn = f'{self.cfg["paths"]["out"]}\\itemworth-{time.strftime("%Y%m%d-%H%M%S")}.png'
        img = QtGui.QGuiApplication.primaryScreen().grabWindow(self.panel.panel.winId())
        w, h = img.size().toTuple()
        start = self.panel.get_item('Start')
        offs = start.pos().y() + start.size().height() + 3
        simg = img.copy(0, offs, w, h - offs) 
        simg.save(fn)
        self.api.log(f'Saved to {fn}')
        