"""
PoEHelper general info plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import glob
import json
import os
import re

from poehelper.poeh_plugins import PoehPlugin



class GeneralInfoPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        'open_reference_on_ctrlc': True,
        }


    def refmap_from_md(self, filename):
        """
        Creates a reference mapping from an MD file.
        """
        text = open(filename).read()
        panels = {}
        cat = desc = None
        for level, name, chunk in re.findall(r'(?:^|\n)(##*)\s*([^\n]+)\r?\n(.*?)(?=\n#|$)', text, flags=re.S):
            if len(level) == 1:
                cat = name.strip()
                desc = chunk.strip()
            else:
                panels[name.strip()] = chunk.strip()
        return cat, panels, desc


    def init(self):
        """
        Initialize the plugin.
        """
        for f in glob.glob(f'{self.paths.doc}/reference/*'):
            panels = None
            if os.path.isfile(f) and f.endswith('.md'):
                cat, panels, desc = self.refmap_from_md(f)
            elif os.path.isdir(f):
                cat = os.path.basename(f)
                desc = None
                panels = {}
                for ff in glob.glob(f'{f}/*.md'):
                    scat, spanels, desc = self.refmap_from_md(ff)
                    panels[scat] = spanels
            if panels:
                self.api.add_reference_panel(cat, panels=panels, description=desc, cols=3)
        for f in glob.glob(f'{self.paths.doc}/json_reference/*.reference.json'):
            data = json.load(open(f))
            self.api.add_reference_panel(data.get('title'), panels=data.get('items', {}),
                    description=data.get('description'), reference=data.get('reference'),
                    or_regex=data.get('or_regex'), cols=4, tagged=True)


    def ingame_ctrlc_handler(self, info, text, **kwargs):
        if 'gem' in info['types'] and self.cfg['open_reference_on_ctrlc']:
            name = info['types'][1].replace(' Support', '')
            self.api.show_reference_panel('Gems', name)

