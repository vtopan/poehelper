"""
PoEHelper syndicate info plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import re

from poehelper.poeh_plugins import PoehPlugin


SYN_RX = re.compile('Aisling|Cameria|Elreon|Gravicius|Guff|Haku|Hillock|It That Fled|Janus|Jorgin'
        '|Korell|Leo|Riker|Rin|Tora|Vagan|Vorici')


class SyndicateInfoPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        'hotkey': 'alt+e',
        }


    def init(self):
        """
        Initialize the plugin.
        """
        self.api.add_game_hotkey(self.cfg['hotkey'], self.show_syn_info)
        self.syn_in_area = []


    def area_changed_handler(self, earg, **kwargs):
        """
        Used to keep track of syn members who talked since area changes.
        """
        self.syn_in_area = []


    def local_handler(self, msg, sender, **kwargs):
        """
        Local message handler - keep track of which syn members talk in the current area.
        """
        m = SYN_RX.search(sender)
        if m:
            name = m.group()
            if name not in self.syn_in_area:
                self.syn_in_area.append(name)


    def show_syn_info(self):
        """
        Check the screen for syndicate names, then expand info if any.
        """
        if self.syn_in_area:
            self.api.log(f'Syndicate members: {", ".join(self.syn_in_area)}')
            self.api.show_reference_panel('Syndicate', subpanels=self.syn_in_area)
            self.api.temporarily_expand()
        else:
            self.api.err('No syndicate members talked in this area!')





