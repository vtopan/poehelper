"""
PoEHelper item info plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import json
import re

from poehelper.poeh_plugins import PoehPlugin

NOTABLE_INFO = None


class ItemInfoPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        }

    def init(self):
        self.chaos_img = self.api.get_image_string('currency', 'Chaos Orb')


    def ingame_ctrlc_handler(self, info, text, **kwargs):
        """
        Handle in-game Ctrl+C key presses.
        """
        global NOTABLE_INFO
        res = []
        attrs = info.get('attributes', {})
        info['mods'] = [e.replace(' (crafted)', '') for e in info.get('mods', [])]
        # DPS
        aps = float(attrs.get('Attacks per Second', 0))
        if aps:
            qual = int(attrs.get('Quality', '0').strip('+%'))
            tdps, cntdps = 0, 0
            for n in ('Physical', 'Elemental', 'Chaos'):
                dam = attrs.get(f'{n} Damage')
                if dam:
                    # elemental damage has multiple values, e.g. '1-3, 7-10'
                    dam = [int(x) for x in re.split('[-, ]+', dam)]
                    dps = (aps * sum(dam) / len(dam))
                    tdps += dps
                    cntdps += 1
                    res.append(f'{n[0]}DPS {dps:.02f}')
            if cntdps > 1:
                res.insert(len(res) - cntdps, 'DPS {tdps:.02f}')
        # life
        mods = {}
        for mod in info.get('mods', []):
            m = re.search(r'^\+(\d+) to (Strength(?: and \w+)?|maximum Life)$', mod)
            if m:
                amt, cat = m.groups()
                amt = int(amt)
                if cat.startswith('Strength'):
                    amt = amt / 2
                mods['life'] = mods.get('life', 0) + amt
        res += sorted('%s: %s' % (k, v) for (k, v) in mods.items())
        # resistances
        mods = {}
        for mod in info.get('mods', []):
            m = re.search(r'^\+(\d+)% to (\w+(?: and \w+)?|all Elemental) Resistances?$', mod)
            if m:
                amt, rts = m.groups()
                rts = ('Cold', 'Fire', 'Lightning') if rts.startswith('all Ele') else rts.split(' and ')
                for restype in rts:
                    k = f'{restype.lower()}-res'
                    mods[k] = mods.get(k, 0) + int(amt)
        # group equal resistances
        for k, v in list(mods.items()):
            if k not in mods:
                continue
            if list(mods.values()).count(v) > 1:
                ks = [e[0] for e in mods.items() if e[1] == v]
                for e in ks:
                    del mods[e]
                mods['+'.join(e.split('-')[0] for e in ks) + '-res'] = v
        if len(mods) > 1:
            mods['total-res'] = sum(v * (1 + k.count('+')) for k, v in mods.items())
        res += sorted(['%s: <b>%s</b>' % (k, v) for (k, v) in mods.items()])
        ## allocated (amulets)
        m = re.search('\nAllocates (.+) \(', text)
        if m:
            notable = m.groups()[0]
            if not NOTABLE_INFO:
                NOTABLE_INFO = json.load(open(f'{self.paths.data}/json/notables.json'))
            ni = NOTABLE_INFO[notable]
            url = self.api.make_evtlog_ref_url('Notables', notable)
            res.append(f'notable: {self.api.make_link(notable, url)} ~<b>{ni["price"]}</b>{self.chaos_img}')
        # cluster jewel
        if 'Cluster Jewel' in info['types']:
            m = re.search('Added Passive Skill is (.+?)( \(|$)', text, flags=re.M)
            if m:
                notable = m.groups()[0]
                if notable not in ['a Jewel Socket']:
                    if not NOTABLE_INFO:
                        NOTABLE_INFO = json.load(open(f'{self.paths.data}/json/notables.json'))
                    ni = NOTABLE_INFO[notable]
                    url = self.api.make_evtlog_ref_url('Notables', notable)
                    res.append(f'notable: {self.api.make_link(notable, url)} ~<b>{ni["price"]}</b>{self.chaos_img}')
        self.api.cc_item_info += res
