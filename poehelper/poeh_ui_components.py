"""
PoEHelper - UI components.

Author: Vlad Topan (vtopan/gmail)
"""

import re

from PySide2 import QtWidgets, QtGui
from PySide2.QtWidgets import QGroupBox, QPushButton, QLabel, QTextEdit, QGridLayout, QHBoxLayout, \
    QVBoxLayout, QWidget, QCheckBox, QTextBrowser, QLineEdit, QComboBox
from PySide2.QtCore import Qt, Signal, Slot, QCoreApplication

from .poeh_logging import err
from .poeh_utils import make_link


FIELD_MAP = {
    'L': QLabel,
    'T': QTextEdit,
    'B': QPushButton,
    'C': QComboBox,
    'I': QLineEdit,
    }


class UiIndicator(QLabel):
    """
    On/off indicator.
    """
    def __init__(self, state=0, labels=('off', 'on'), colors=('red', 'green'), hide_when_off=False):
        super().__init__()
        self.base_style = 'UiIndicator {font: bold 8pt Fontin; color: white; text-align: center;}\n'
        self.labels = labels
        self.colors = colors
        self.hide_when_off = hide_when_off
        self.setAutoFillBackground(True)
        self.setFixedHeight(20)
        self.setFixedWidth(24)
        self.setAlignment(Qt.AlignCenter)
        self.setIndState(bool(state))


    def setIndState(self, state=0):
        """
        Set the current state (on/off).

        :param state: 0 = off, 1 = on, -1 = toggle.
        """
        if state == -1:
            state = not(self.state)
        self.state = state
        if type(state) is str:
            color = self.colors[0]
            text = self.state
        else:
            color = self.colors[state]
            text = self.labels[state]
        self.setStyleSheet(self.base_style + 'UiIndicator {background-color: %s;}' % color)
        self.setText(text)
        if self.hide_when_off and not self.state:
            self.hide()
        else:
            self.show()


class Button(QPushButton):
    """
    Custom button.
    """

    def __init__(self, *args, height=None, width=None, **kwargs):
        super().__init__(*args, **kwargs)
        if height:
            self.setFixedHeight(height)
        if width:
            self.setFixedWidth(width)



class HorizontalLine(QtWidgets.QFrame):

    def __init__(self):
        # todo: fixme
        super().__init__()
        self.setStyleSheet('* {border: solid 1px white}')
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)



class ToggleGroup(QGroupBox):
    """
    Toggle-able panels (categories) controlled by a set of checkboxes.

    :param description: The contents of the first (always-on) panel.
    :param panels: A dict-like mapping of categories to widgets.
    :param notify_on_resize: Widget to notify (call its .autosize()) when a panel is hidden/shown.
    """

    show_panel_sig = Signal(object, bool)


    def __init__(self, title, *args, description=None, panels=None, cols=4, notify_on_resize=None, **kwargs):
        super().__init__(*args, title=title, **kwargs)
        self.notify_on_resize = notify_on_resize
        self.title = title
        self.buttons = {}
        self.panels = {}
        self.cols = cols
        lay = self.lay = QVBoxLayout()
        lay.setContentsMargins(2, 8, 2, 2)
        self.hdr_lines = []
        self.horiz_lines = []
        self.desc = None
        self.break_next = False
        if description:
            self.set_description(description)
        if panels:
            for grp, panel in panels.items():
                self.add_panel(grp, panel)
        self.setLayout(lay)
        self.show_panel_sig.connect(self.show_panel_slot)


    def open_panels(self):
        """
        Returns a list of open panel names.
        """
        return [k for k, v in self.buttons.items() if v.isChecked()]


    def set_description(self, html_or_widget):
        """
        Set the contents of the group description panel.
        """
        if type(html_or_widget) is str:
            self.desc = QTextEdit(self)
            self.desc.setReadOnly(True)
            self.desc.insertHtml(html_or_widget)
        else:
            self.desc = html_or_widget
        fun = self.lay.insertWidget if self.desc.isWidgetType() else self.lay.insertLayout
        fun(len(self.hdr_lines), self.desc)


    def add_panel(self, name, widget):
        """
        Add a panel to the group.

        :param name: Panel name.
        :param widget: A Qt widget or None to add a line break.
        """
        if not widget:
            self.horiz_lines.append(HorizontalLine())
            self.lay.addWidget(self.horiz_lines[-1])
            self.break_next = True
            return
        self.buttons[name] = hcb = QPushButton(name)
        self.panels[name] = widget
        hcb.setCheckable(True)
        hcb.setObjectName('toggle')
        hcb.setFixedHeight(24)
        hcb.setMaximumWidth(self.width() // self.cols)
        hcb.clicked.connect(self.header_clicked)
        # find horiz. layout in header to which this hcb belongs
        if not self.hdr_lines or self.hdr_lines[-1].count() == self.cols or self.break_next:
            lay = QHBoxLayout()
            self.lay.insertLayout(len(self.hdr_lines), lay)
            self.hdr_lines.append(lay)
            self.break_next = False
        lay = self.hdr_lines[-1]
        lay.addWidget(hcb)
        self.lay.addWidget(widget)
        widget.hide()


    def show_panel(self, name, show=-1):
        """
        Show/hide a panel (default: toggle).
        """
        show = not self.panels[name].isVisible() if show == -1 else bool(show)
        self.buttons[name].setChecked(show)
        self.show_panel_sig.emit(name, show)


    def show_panels(self, names):
        """
        Show (only) the given list of panels.
        """
        show, hide = [], []
        for p in self.panels:
            if p in names and not self.panels[p].isVisible():
                show.append(p)
            elif p not in names and self.panels[p].isVisible():
                hide.append(p)
            self.buttons[p].setChecked(p in names)
        self.show_panel_sig.emit(hide, False)
        self.show_panel_sig.emit(show, True)


    @Slot(object, bool)
    def show_panel_slot(self, names, show):
        """
        Show or hide a panel - slot.
        """
        names = names if type(names) in (list, tuple) else [names]
        for name in names:
            self.panels[name].setVisible(show)
        # self.panels[name].setFixedHeight(self.panels[name].minimumSizeHint().height())
        self.autosize()


    def hide_all_panels(self):
        """
        Hide (close) all panels.
        """
        for k, v in self.buttons.items():
            if v.isChecked():
                self.show_panel(k, False)


    def header_clicked(self):
        """
        Show/hide panel when its name is clicked.
        """
        panel = self.sender().text()
        show = self.sender().isChecked()
        self.show_panel_sig.emit(panel, show)


    def autosize(self):
        qtapp = QCoreApplication.instance()
        for i in range(10):
            qtapp.processEvents()
        self.setMinimumHeight(self.sizeHint().height())
        if self.notify_on_resize:
            self.notify_on_resize.autosize()



class PluginPanel(QGroupBox):
    """
    On/off panel configured by a plugin.

    :param fields_as_str: ASCII drawing of layout.
    :param field_names: If `fields_as_str` is set, this can be used to map field ids to names.
    :param fields: Fields as lists of lists.
    """

    set_item_text_sig = Signal(int, int, object, str)
    add_grid_cell_sig = Signal(int, list)
    set_button_click_sig = Signal(QPushButton, object)
    set_onchange_sig = Signal(object, object)


    def __init__(self, title, fields=None, fields_as_str=None, field_names=None):
        super().__init__(title=title)
        # layouts/widgets: self < scroll_lay1 < scroll_area < scroll_lay2 < panel < lay
        self.scroll_lay1 = QVBoxLayout()
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_lay2 = QVBoxLayout()
        self.panel = QtWidgets.QWidget()
        self.lay = QVBoxLayout()

        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.panel)
        self.scroll_area.setLayout(self.scroll_lay2)
        self.scroll_lay1.setContentsMargins(0, 5, 0, 0)
        self.scroll_lay1.addWidget(self.scroll_area)
        self.scroll_lay2.setContentsMargins(0, 0, 0, 0)
        self.scroll_lay2.addWidget(self.panel)
        self.lay.setContentsMargins(2, 8, 2, 2)
        self.panel.setLayout(self.lay)
        self.setLayout(self.scroll_lay1)

        self.field_rx = re.compile(rf'([{"".join(FIELD_MAP.keys())}\.])(?:<(.+?)>)?')
        self.grid_rx = re.compile(r'^([\[\{])(\|?)(.+?)(?:[\]\}])(?:<(.+?)>)?$')
        if fields_as_str:
            self.ui_from_string(fields_as_str)
        else:
            self.fields = fields
            raise NotImplementedError("...")
        self.field_names = field_names or {}
        self.callbacks = []
        self.set_item_text_sig.connect(self.set_item_text_slot)
        self.add_grid_cell_sig.connect(self.add_grid_cell_slot)
        self.set_button_click_sig.connect(self.set_button_click_slot)
        self.set_onchange_sig.connect(self.set_onchange_slot)


    def get_item(self, field, cell=None, grid=None):
        """
        Get the a UI component from the panel as a Qt widget.
        """
        if cell is not None:
            # grid item
            grid_widget = self.grid(grid)
            grid = grid_widget.id
            if type(field) is str:
                field = grid_widget.map[field]
            item = self.grid_item(cell=cell, field=field, grid=grid)
        else:
            # non-grid item
            cell = -2
            item = (self.field_map if type(field) is str else self.fields)[field]
        return item


    def get_item_text(self, field, cell=None, grid=None):
        """
        Get the text of a UI component.
        """
        item = self.get_item(field, cell=cell, grid=grid)
        if type(item) in (QPushButton, QTextEdit, QLabel, QLineEdit):
            return item.text()
        elif type(item) in (QComboBox,):
            return item.currentText()


    def set_item_text(self, field, text, cell=None, grid=None):
        """
        Set the text of a UI component.
        """
        text = str(text)
        if cell is not None:
            # grid item
            grid_widget = self.grid(grid)
            grid = grid_widget.id
            if type(field) is str:
                field = grid_widget.map[field]
        else:
            # non-grid item
            cell = -2
            field = (self.field_map if type(field) is str else self.fields)[field]
        self.set_item_text_sig.emit(cell, grid, field, text)


    @Slot(int, int, object, str)
    def set_item_text_slot(self, cell, grid, field, text):
        """
        Slot - set the text of a UI component.
        """
        if cell != -2:
            item = self.grid_item(cell=cell, field=field, grid=grid)
        else:
            item = field
        if type(item) in (QPushButton, QTextEdit, QLabel, QLineEdit):
            item.setText(text)
        elif type(item) in (QComboBox,):
            if '|' in text:
                item.addItems(text.split('|'))
            else:
                item.setCurrentText(text)
        else:
            raise ValueError(f'Unknown item type {type(item)}!')
        self.autosize()


    def ui_from_string(self, s):
        """
        Parse a string containing a UI design, line by line.

        Line syntax:
            - one or more `field-type<field-name>` per line (field-name is optional)
                - field type is `L` (label), `B` (button), `T` (editable text) or `.` (empty space)
            - for a grid of same-type fields put the field list in curly brackets `{...}`
                - if only one per line and table-like, use right brackets `[...]` ("basic" grid)
                - appending `<name>` is optional

        Example lines:

            - `TB<Click Me>` produces a text box and a button called "Click Me"
            - `[L<name>L<value>]` produces a vertical list of cells, each containing two labels:
              `name` and `value`

        All non-grid fields are numbered sequentially from 0 and can be accessed through that ID.
        Grids themselves have an ID, their cells can be addressed by the cell's sequential number,
        and the field inside the cell by its name or sequential number per cell.
        """
        self.fields = []
        self.grids = []
        self.field_map = {}
        for line in s.split('\n'):
            line = line.strip()
            if not line:
                continue
            m = self.grid_rx.search(line)
            if m:
                gtype, gmods, pattern, name = m.groups()
                grid = QGridLayout()
                self.grids.append(grid)
                grid.mods = gmods or ''
                if '|' in gmods:
                    grid.setHorizontalSpacing(0)
                    grid.setVerticalSpacing(0)
                if name:
                    self.field_map[name] = grid
                pattern = self.field_rx.findall(line[1:-1].strip())
                grid.pattern = pattern
                grid.basic = gtype == '['
                if gtype == '{':
                    grid.cols = 3      # todo: make configurable
                grid.cnt = 0
                grid.map = {name:i for i, (_, name) in enumerate(pattern) if name}
                grid.id = len(self.grids) - 1
                self.lay.addLayout(grid)
            else:
                fields = self.field_rx.findall(line)
                if not fields:
                    raise ValueError(f'No fields found on line {line}!')
                lay_line = QHBoxLayout()
                for ftype, fname in fields:
                    if ftype == '.':
                        lay_line.addStretch()
                        continue
                    field = FIELD_MAP[ftype]()
                    if ftype == 'C':
                        field.setInsertPolicy(QComboBox.NoInsert)
                    self.fields.append(field)
                    if fname:
                        self.field_map[fname] = field
                        if ftype == 'B':
                            field.setText(fname)
                    lay_line.addWidget(field)
                self.lay.addLayout(lay_line)


    def grid(self, grid=None):
        """
        Get a grid by name or ID (default: the first grid).

        :param grid: Grid name, ID number or None = first grid.
        """
        if grid is None or type(grid) is int:
            grid_id = int(grid or 0)
        elif type(grid) == QGridLayout:
            grid_id = grid.id
        else:
            grid_id = self.field_map[grid]
        grid = self.grids[grid_id]
        return grid


    def clear_grid(self, grid=None):
        """
        Remove all grid contents.
        """
        grid_widget = self.grid(grid)
        while grid_widget.count():
            child = grid_widget.takeAt(0)
            if child.widget():
                child.widget().deleteLater()


    def add_grid_cell(self, *args, grid=None, **kwargs):
        """
        Add a grid cell - *args are the values.

        :param grid_id: Grid name, ID number or None = first grid.
        """
        args = [str(x) for x in args]
        grid_widget = self.grid(grid)
        fields = []
        for i, (ftype, fname) in enumerate(grid_widget.pattern):
            if len(args) > i:
                value = args[i]
            else:
                value = kwargs.get(fname, None)
            fields.append(value)
        self.add_grid_cell_sig.emit(grid_widget.id, fields)


    @Slot(int, list)
    def add_grid_cell_slot(self, grid_id, fields):
        """
        Add a grid cell - *args are the values.

        :param grid_id: Grid name, ID number or None = first grid.
        """
        grid_widget = self.grid(grid_id)
        num = grid_widget.cnt
        grid_widget.cnt += 1
        if not grid_widget.basic:
            x, y = num // grid_widget.cols, num % grid_widget.cols
            cell = QHBoxLayout()
            grid_widget.addLayout(cell, x, y)
        for i, (ftype, fname) in enumerate(grid_widget.pattern):
            value = fields[i]
            field = FIELD_MAP[ftype]()
            if '|' in grid_widget.mods:
                field.setStyleSheet('* {border: 1px solid #310;}')
            if grid_widget.basic:
                grid_widget.addWidget(field, num, i)
            else:
                cell.addWidget(field)
            if value:
                self.set_item_text(i, value, cell=num, grid=grid_widget)
        self.autosize()


    def autosize(self):
        # self.panel.setFixedHeight(self.lay.sizeHint().height())
        # self.scroll_area.setFixedHeight(self.scroll_lay2.sizeHint().height())
        # self.setFixedHeight(self.scroll_lay1.sizeHint().height())
        # self.parent().autosize()
        sb = self.scroll_area.horizontalScrollBar()
        delta = sb.height() if sb.isVisible() else 0
        self.panel.setMinimumHeight(self.lay.sizeHint().height())
        self.scroll_area.setMinimumHeight(self.scroll_lay2.sizeHint().height() + delta)
        self.setMinimumHeight(self.scroll_lay1.sizeHint().height())
        self.parent().autosize()


    def grid_item(self, cell, field, grid=None):
        """
        Get a grid item widget by cell number and field name/number.
        """
        grid = self.grid(grid)
        if cell == -1:
            cell = grid.cnt - 1
        if type(field) is str:
            field = grid.map[field]
        if grid.basic:
            widget = grid.itemAtPosition(cell, field).widget()
        else:
            cell = grid.itemAtPosition(cell // grid.cols, cell % grid.cols)
            widget = cell.itemAt(field).widget()
        return widget


    def set_grid_item(self, cell, field, value, grid=None):
        """
        Set the value of an item inside a grid cell.

        :param cell: The (sequential) field number of the cell.
        :param field: The ID or name of the field inside the cell.
        :param value: The value to set.
        :param grid: Grid name, ID number or None = first grid.
        """
        self.set_item_text(field, value, cell=cell, grid=grid)


    def set_button_click(self, field, callback, cell=None, grid=None):
        """
        Set the callback for clicking on a button.
        """
        if cell is not None:
            button = self.grid_item(cell, field, grid)
        else:
            button = self.field_map.get(field) or self.fields[field]
        self.callbacks.append(callback)
        self.set_button_click_sig.emit(button, callback)


    @Slot(QPushButton, object)
    def set_button_click_slot(self, button, callback):
        """
        Slot - set the callback for clicking on a button.
        """
        button.clicked.connect(callback)


    def set_onchange(self, field, callback, cell=None, grid=None):
        """
        Set the callback for when an item changes contents.
        """
        if cell is not None:
            item = self.grid_item(cell, field, grid)
        else:
            item = self.field_map.get(field) or self.fields[field]
        self.callbacks.append(callback)
        self.set_onchange_sig.emit(item, callback)


    @Slot(object, object)
    def set_onchange_slot(self, item, callback):
        """
        Slot - set the callback for when an item changes contents.
        """
        if item.__class__ in (QCheckBox,):
            item.currentTextChanged.connect(callback)
        elif item.__class__ in (QLineEdit, QTextEdit):
            item.textChanged.connect(callback)
        else:
            err(f'Can\'t set on-change handler for {item.__class__.__name__}!')



class TaggedReferencePanel(QWidget):
    """
    Panel which displays filtered data based on tags.

    :param info: Dict mapping name to (text, tag_list).
    """


    def __init__(self, name, panels, description=None, cols=4, notify_on_resize=None, parser=None,
            reference=None, or_regex=None):
        super().__init__()
        self.name = name
        self.parser = parser
        self.or_regex = or_regex and re.compile(or_regex)
        self.notify_on_resize = notify_on_resize
        self.cb_lay = QGridLayout()
        self.cbs = {}
        self.cols = cols
        self.panels = panels
        self.tagmap = {}
        self.taggroup = {}
        self.checked_tags = []
        self.description = description
        self.active_panel = None
        for k, v in self.panels.items():
            for t in v['tags']:
                if t not in self.tagmap:
                    self.tagmap[t] = set()
                self.tagmap[t].add(k)
            if parser:
                v['text'] = parser(v['text'].strip())
        self.tags = sorted(self.tagmap.keys(), key=lambda x:re.sub(r'\d+', 
                lambda m:'%06d' % int(m.group()), x))
        tg = {}  # temporary tag grouping dict
        for i, t in enumerate(self.tags):
            cb = self.cbs[t] = QCheckBox(t)
            cb.stateChanged.connect(self.checkbox_toggled)
            self.cb_lay.addWidget(cb, i // cols, i % cols)
            if self.or_regex and self.or_regex.search(t):
                tgen = self.or_regex.sub(lambda m:m.group().replace(m.group(1), 'X'), t)
                if tgen not in tg:
                    tg[tgen] = set()
                tg[tgen].add(t)
                self.taggroup[t] = tg[tgen]
        self.item_list = QTextBrowser()
        self.item_list.setOpenLinks(False)
        self.item_list.anchorClicked.connect(self.link_clicked)
        self.item = QTextBrowser()
        self.item.setOpenExternalLinks(True)
        self.item.setVisible(False)
        self.ref = None
        if reference:
            self.ref_lay = QHBoxLayout()
            self.ref = QLabel()
            self.ref_close = QPushButton('x')
            self.ref_close.clicked.connect(self.hide_reference)
            self.ref_close.setFixedWidth(20)
            self.ref_lay.addWidget(self.ref_close)
            self.ref_lay.addWidget(self.ref)
            if parser:
                reference = parser(reference)
            self.ref.setText('<b>Reference:</b><br>' + reference)
            self.ref.setFixedHeight(self.ref.sizeHint().height())
            self.ref.setOpenExternalLinks(True)
            self.ref.linkActivated.connect(self.ref.hide)
        self.lay = QVBoxLayout()
        self.lay.addLayout(self.cb_lay)
        self.lay.addWidget(self.item_list)
        self.lay.addWidget(self.item)
        if self.ref:
            self.lay.addLayout(self.ref_lay)
        self.setLayout(self.lay)
        self.item_list_from_search = False
        self.checkbox_toggled()


    def open_panels(self):
        """
        Returns a list of open panel names (max. 1).
        """
        return [self.active_panel] if self.active_panel else []


    def set_item_list(self, values, msg=None, from_search=False):
        """
        Fill in the item list with links.
        """
        self.show()
        self.item_list.clear()
        if msg:
            self.item_list.setHtml(msg)
        else:
            self.item_list.setHtml(', '.join(make_link(e, e) for e in sorted(values)))
        self.item_list_from_search = from_search
        self.item_list.setFixedHeight(min(150,
                4 + self.item_list.document().documentLayout().documentSize().height()))
        self.autosize()


    def checkbox_toggled(self, state=None):
        """
        A checkbox changed state.
        """
        tags = self.checked_tags = [k for k, v in self.cbs.items() if v.isChecked()]
        msg = None
        if not tags:
            msg = 'No tag selected.'
        ids = []
        for t in list(tags):
            if t in self.taggroup:
                tag_group = self.taggroup[t] & set(tags)
                ids.append(set.union(*[self.tagmap[t] for t in tag_group]))
            else:
                ids.append(self.tagmap[t])
        if ids:
            ids = set.intersection(*ids)
        if not ids and msg is None:
            msg = 'No items matched.'
        self.set_item_list(ids, msg=msg)


    def show_panel(self, panel=None, show=True):
        """
        Display the text of the given panel.
        """
        self.show()
        self.item.clear()
        self.item.setVisible(show)
        if panel:
            info = self.panels[panel]
            self.item.setHtml(f'<center><b>{panel}</b></center>' + info['text'])
            self.item.setFixedHeight(2 +
                self.item.document().documentLayout().documentSize().height())
            self.autosize()
            self.active_panel = panel


    def link_clicked(self, url):
        """
        Show clicked panel (item).
        """
        item = url.path()
        self.show_panel(item)


    def find_text(self, text):
        """
        Show items containing given text; hide if none.

        :return: Count of matching subpanels.
        """
        panels = []
        for k, v in sorted(self.panels.items()):
            if text in v['text'].lower() or text in k.lower():
                panels.append(k)
        self.set_item_list(panels, from_search=True)
        if panels:
            self.show_panel(panels[0])
        if hasattr(self.notify_on_resize, 'show_panel'):
            self.notify_on_resize.show_panel(self.name, bool(panels))


    def hide_reference(self):
        """
        Hide reference clicked.
        """
        self.ref.hide()
        self.ref_lay.removeWidget(self.ref)
        self.ref_close.hide()
        self.ref_lay.removeWidget(self.ref_close)
        self.autosize()


    def autosize(self):
        qtapp = QCoreApplication.instance()
        for i in range(10):
            qtapp.processEvents()
        self.setMinimumHeight(self.sizeHint().height())
        if self.notify_on_resize:
            self.notify_on_resize.autosize()



class ReferencePanel(ToggleGroup):
    """
    Reference panel.
    """

    def __init__(self, name, panels, description=None, cols=4, notify_on_resize=None, parser=None,
            reference=None):
        self.name = name
        self.parser = parser
        widgets, self.text_panels = {}, {}
        self.subpanels = {}
        self.text_cache = {}
        for k, v in panels.items():
            if not v:
                widgets[k] = None
                continue
            widget = self.make_panel(k, v)
            widgets[k] = widget
            (self.text_panels if widget.__class__ == QTextBrowser else self.subpanels)[k] = widget
        if description:
            description = QtWidgets.QLabel(description)
        if reference:
            if parser:
                reference = parser(reference)
            widgets['Reference'] = self.make_panel('Reference', reference)
        super().__init__(name, description=description, panels=widgets, cols=cols,
                notify_on_resize=notify_on_resize)
        for name, widget in self.text_panels.items():
            text = widget.toPlainText().lower()
            self.text_cache[name] = text
            linecount = text.count('\n') + 1
            rowheight = QtGui.QFontMetrics(widget.font()).lineSpacing()
            height = 10 + linecount * rowheight
            widget.setFixedHeight(height)
        self.autosize()


    def make_panel(self, name, info):
        """
        Create a reference widget or ReferencePanel.
        """
        if type(info) is dict:
            panel = ReferencePanel(name, panels=info, cols=self.cols, notify_on_resize=self)
            return panel
        elif type(info) is list:
            info = '<br>'.join(info)
        w = QtWidgets.QTextBrowser()
        w.setOpenExternalLinks(True)
        w.document().setDefaultStyleSheet('a:hover {color:#AA0;text-decoration:none;} table {border: 1px solid #530;}')
        w.setFocusPolicy(Qt.NoFocus)
        text = info.strip()
        if self.parser:
            text = self.parser(text)
        w.setHtml(f'<center><b>{name}</b></center>' + text)
        return w


    def find_text(self, text):
        """
        Show panels containing given text; hide if none.

        :return: Count of matching subpanels (recursively).
        """
        found = 0
        text = text.lower()
        for panel in self.subpanels.values():
            found += panel.find_text(text)
        new_panels = []
        open_panels = self.open_panels()
        for panel, ptext in self.text_cache.items():
            if text in ptext or text in panel.lower():
                new_panels.append(panel)
                if panel not in open_panels:
                    self.show_panel(panel, True)
        for panel in open_panels:
            if panel not in new_panels:
                self.show_panel(panel, False)
        found += len(new_panels)
        if hasattr(self.notify_on_resize, 'show_panel'):
            self.notify_on_resize.show_panel(self.name, found != 0)
        return found



