"""
PoEHelper: constants and misc data.

Author: Vlad Topan (vtopan/gmail)
"""

import json
import os
import re
import shutil
import time

import requests
from PIL import Image

from iopoelib.net import poecom_get_trade_static_data, poecom_extract_trademap

from .poeh_logging import dbg, log


LOG_COLOR_MAP = {'!': 'red', '#': '#666', '*': '#752'}
TOWNS = ('Lioneye\'s Watch', 'The Forest Encampment', 'The Sarn Encampment', 'Highgate',
        'Overseer\'s Tower', 'The Bridge Encampment', 'Oriath Docks', 'Oriath')
_TRD_MAPPING = None


def get_poecom_static_data(cache_path, max_age=24 * 3600):
    """
    Get the (cached) static data from pathofexile.com/trade.
    """
    if not os.path.isdir(cache_path):
        os.makedirs(cache_path)
    cache_file = os.path.join(cache_path, 'poecom-trade.json')
    if not os.path.isfile(cache_file) or time.time() - os.path.getmtime(cache_file) > max_age:
        dbg('Downloading static trade data from pathofexile.com/trade...')
        jdata = poecom_get_trade_static_data()
        if os.path.isfile(cache_file):
            os.remove(cache_file)
        json.dump(jdata, open(cache_file, 'w'), indent=2, sort_keys=1)
    else:
        jdata = json.load(open(cache_file))
    return jdata


def get_trade_mapping(cache_path, max_age=24 * 3600, inverted=False):
    """
    Get mapping from currency/scarab/fragment/map name to (short_name, image_url).
    """
    global _TRD_MAPPING, _TRD_MAPPING_INV
    if _TRD_MAPPING:
        return _TRD_MAPPING_INV if inverted else _TRD_MAPPING
    cache_file = os.path.join(cache_path, 'poecom-trademap.json')
    if not os.path.isfile(cache_file) or time.time() - os.path.getmtime(cache_file) > max_age:
        jdata = poecom_extract_trademap(get_poecom_static_data(cache_path), include_type=True)
        if os.path.isfile(cache_file):
            os.remove(cache_file)
        json.dump(jdata, open(cache_file, 'w'), indent=2, sort_keys=1)
    else:
        jdata = json.load(open(cache_file))
    _TRD_MAPPING = jdata
    _TRD_MAPPING_INV = {v[0]:k for k, v in _TRD_MAPPING.items()}
    return _TRD_MAPPING_INV if inverted else _TRD_MAPPING


def get_image_path(cache_path, itemtype, itemname, ext='png'):
    """
    Return the image cache path for an item type/name.
    """
    path = f'{cache_path}/images/{itemtype}'
    if not os.path.isdir(path):
        os.makedirs(path)
    pitemname = re.sub(r'[^a-z0-9_-]+', '_', itemname.lower())
    ipath = f'{path}/{pitemname}.{ext}'
    if not os.path.isfile(ipath) or os.path.getsize(ipath) == 0:
        if itemtype not in ('currency', 'scarab', 'fragment', 'trade', 'map', 'oil') and not itemtype.startswith('mapstier'):
            raise NotImplementedError(f'Don\'t know how to get image for {itemtype}:{itemname}!')
        cmap = get_trade_mapping(cache_path)
        if itemname not in cmap:
            raise ValueError(f'Unknown trading item {itemname}!')
        url = cmap[itemname][1]
        if not url:
            return None
        req = requests.get(url, stream=True)
        if req.status_code != 200:
            raise ValueError(f'Failed downloading [{url}]: {req.status_code}!')
        req.raw.decode_content = True
        with open(ipath, 'wb') as fh:
            shutil.copyfileobj(req.raw, fh)
        # crop (mostly) transparent border
        im = Image.open(ipath)
        size = im.size
        crop = 6
        im = im.crop((crop, crop, size[0] - crop, size[1] - crop))
        # im = im.crop(im.getbbox())
        im.save(ipath)
    return ipath


