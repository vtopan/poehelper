"""
PoEHelper - event generator / processor.

Author: Vlad Topan (vtopan/gmail)
"""

import html
import datetime
import threading
import time

from iopoelib import app as poeapp, misc as poemisc

from .poeh_constants import TOWNS
from .poeh_logging import log, err, dbg
from .poeh_utils import adict, hotkey, read_clipboard


class EventMgr:
    """
    Event generator and handler.
    """

    def __init__(self, app):
        super().__init__()
        self.app = app
        app.state = self.state = adict(game_running=None, area=None, in_town=None, in_hideout=None)
        self.stop = False
        self.threadcnt = 0
        self.threads = adict()
        self.options = app.cfg.options
        self.poll_freq = self.options.poll_frequency
        self.run_threaded('watch_game', self.watch_game_state)
        if self.options.watch_client_txt:
            self.run_threaded('client_txt', self.watch_client_txt)
        hotkey('Ctrl+C', self.watch_game_ctrlc, suppress=False)


    def run_threaded(self, name, fun, args=tuple()):
        """
        Run a function threaded.
        """
        self.threads[name] = threading.Thread(target=fun, args=args).start()


    def watch_game_state(self):
        """
        Runs threaded; keeps the `self.state.game_running` var updated.
        """
        self.threadcnt += 1
        while not self.stop:
            new = poeapp.game_running()
            if new != self.state.game_running:
                self.state.game_running = new
                if new:
                    self.app.cfg.paths.poe_path = poeapp.get_poe_path()
                self.new_event('game_running', new)
            time.sleep(self.poll_freq)
        self.threadcnt -= 1


    def watch_client_txt(self):
        """
        Runs threaded; watches client.txt for changes.
        """
        self.threadcnt += 1
        while not self.state.game_running:
            if self.stop:
                self.threadcnt -= 1
                return
            time.sleep(self.poll_freq)
        log_path = poeapp.get_log_path()
        dbg(f'Found game log @ {log_path}')
        area = poeapp.current_area(poeapp.read_log_tail(log_path, size=64 * 1024))
        if area:
            self.new_event('area_changed', area, timestamp=datetime.datetime.now())
        gen = poeapp.read_log_line(log_path, seek=self.options.log_tail, parse=True, live=True,
                timestamp=True)     # filter=('area',)
        while not self.stop:
            e = next(gen)
            if e is None:
                time.sleep(self.poll_freq)
                continue
            if 'area' in e:
                self.new_event('area_changed', e['area'], timestamp=e['timestamp'])
            elif 'afk' in e:
                self.new_event('afk', e['afk'] == 'ON')
            elif 'pm' in e:
                e['pm'] = poemisc.shorten_wtb_pm(e['pm'])
                self.new_event('pm', e['pm'], sender=e['pm_sender'], timestamp=e['timestamp'])
            elif 'local' in e:
                self.new_event('local', e['local'], sender=e['local_sender'], timestamp=e['timestamp'])
            else:
                err(f'Unknown event type: {e}')
        self.threadcnt -= 1


    def watch_game_ctrlc(self):
        """
        Watcher for in-game Ctrl+C events.
        """
        if self.game_focused:
            dbg('Ctrl+C pressed in game')
            time.sleep(0.1)     # ensure clipboard is updated
            text = read_clipboard()
            if text and text.startswith('Rarity: ') and '--------' in text:
                parsed = poemisc.parse_ctrlc(text)
                self.new_event('ingame_ctrlc', parsed, text=text)


    @property
    def game_focused(self):
        """
        Checks if game window is focused.
        """
        return poeapp.game_focused()


    def close(self):
        """
        Shutdown the event manager.
        """
        self.stop = True
        i = 0
        while self.threadcnt:
            time.sleep(self.poll_freq)
            i += 1
            if i >= 3:
                break


    def new_event(self, etype, earg, **kwargs):
        """
        Issue a new event.
        """
        dbg(f'new event: {etype}: {html.escape(str(earg))} {html.escape(str(kwargs))}')
        kwargs['source'] = kwargs.get('source') or 'core'
        if etype == 'area_changed':
            area = earg
            self.state.in_hideout = kwargs['in_hideout'] = area.endswith(' Hideout') and \
                    not area.startswith('Syndicate')
            self.state.in_town = kwargs['in_town'] = area in TOWNS
        self.app.ui.handle_event(etype, earg, **kwargs)
        self.app.plug_mgr.handle_event(etype, earg, **kwargs)

