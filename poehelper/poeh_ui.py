#!/usr/bin/env python3
"""
PoEHelper - UI.

Author: Vlad Topan (vtopan/gmail)
"""

try:
    from cgi import escape
except ImportError:
    from html import escape
import os
import re
import time

from PySide2 import QtGui, QtWidgets
from PySide2.QtCore import Qt, SIGNAL, QCoreApplication, Signal, Slot, QTimer

import iolib.win as win

from .poeh_constants import LOG_COLOR_MAP
from .poeh_logging import log, err, dbg
from .poeh_utils import adict, hotkey, StoppableTimer, format_seconds, delayed_run
from .poeh_ui_components import UiIndicator, Button, ToggleGroup, PluginPanel, ReferencePanel, \
    TaggedReferencePanel


class PoehMainWindow(QtWidgets.QMainWindow):
    """
    PoE Helper main window.

    :param app: The PoEHelper (application) instance.
    """

    set_ui_text_sig = Signal(str, str)
    toggle_hide_sig = Signal()
    show_ref_panel_sig = Signal(str, object, bool)


    def __init__(self, app):
        # todo: split properly
        super().__init__()
        self.app = app
        self.qtapp = QCoreApplication.instance()
        self.cfg = app.cfg.ui
        self.hidden = False
        self.icon = QtGui.QIcon(rf'{self.app.cfg.paths.rsrc}\poehelper.ico')
        self.qtapp.setWindowIcon(self.icon)
        if 'winpos' not in self.cfg:
            # center horizontally at the top of the screen
            rect = self.frameGeometry()
            pos = QtWidgets.QDesktopWidget().availableGeometry().center()
            rect.moveCenter(pos)
            rect.setY(0)
            self.move(rect.x(), rect.y())
            self.cfg.winpos = {'min': self.pos().toTuple(), 'max': self.pos().toTuple()}
        else:
            self.move(*self.cfg.winpos['max'])
        # settings
        self.load_resources()
        # UI components
        self.main = QtWidgets.QWidget()     # central widget
        self.lbl = adict()  # labels
        self.ind = adict()  # indicators
        self.but = adict()  # buttons
        self.ind.game_running = UiIndicator(0)
        self.ind.afk = UiIndicator(0, colors=('black', '#AA0'), labels=('', 'afk'), hide_when_off=1)
        self.lbl.area = QtWidgets.QLabel('unknown area')
        self.but.close = Button('x', self, width=20, height=20)
        self.but.close.clicked.connect(self.close)
        if self.cfg.enable_timer:
            self.but.timer = QtWidgets.QPushButton('00:00', self)
            self.but.timer.setFixedSize(60, 24)
            self.but.timer.clicked.connect(self.timer_clicked)
            self.timer = None
        if self.cfg.enable_counter:
            self.but.counter = QtWidgets.QPushButton('0', self)
            self.but.counter.setFixedSize(30, 24)
            self.but.counter.clicked.connect(self.counter_clicked)
            self.but.counter.setContextMenuPolicy(Qt.CustomContextMenu)
            self.but.counter.customContextMenuRequested.connect(self.counter_reset)
            self.counter = 0
        self.but.min = Button('-', self, width=20, height=20)
        self.but.min.clicked.connect(self.toggle_hide)
        self.eventlog = QtWidgets.QTextBrowser()
        self.eventlog.setStyleSheet('QScrollBar:vertical {width: 10px;}')
        self.eventlog.setPlaceholderText('Log messages')
        self.eventlog.setReadOnly(1)
        self.refdesc, self.lay_refdesc = [], QtWidgets.QHBoxLayout()
        self.refdesc.append(QtWidgets.QLabel('Search:'))
        self.refdesc.append(QtWidgets.QLineEdit())
        self.refdesc[-1].textChanged.connect(self.reference_search)
        self.ref_search_timer = None
        self.reference = ToggleGroup('Reference', description=self.lay_refdesc,
                notify_on_resize=self, cols=3)
        for e in self.refdesc:
            self.lay_refdesc.addWidget(e)
        self.open_ref_panels = []
        self.plugins = ToggleGroup('Plugins', notify_on_resize=self, cols=3)
        # layout
        self.lay = QtWidgets.QVBoxLayout()
        self.lay.setContentsMargins(5, 5, 5, 5)
        self.lay_title = QtWidgets.QHBoxLayout()
        self.lay_title.addWidget(self.ind.game_running)
        self.lay_title.addWidget(self.ind.afk)
        self.lay_title.addWidget(self.lbl.area)
        if self.cfg.enable_counter:
            self.lay_title.addWidget(self.but.counter)
        if self.cfg.enable_timer:
            self.lay_title.addWidget(self.but.timer)
        self.lay_title.addWidget(self.but.close)
        self.lay_title.addWidget(self.but.min)
        self.lay.addLayout(self.lay_title)
        self.lay.addWidget(self.reference)
        self.lay.addWidget(self.plugins)
        self.lay.addWidget(self.eventlog)
        self.main.setLayout(self.lay)
        self.setCentralWidget(self.main)
        # shortcuts
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("Esc"), self), SIGNAL('activated()'),
                self.hide_app)
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("Alt+Q"), self), SIGNAL('activated()'),
                self.close)
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("F"), self), SIGNAL('activated()'),
                self.refdesc[-1].setFocus)
        QtWidgets.QShortcut(QtGui.QKeySequence("Alt+F2"), self, lambda:1)
        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+C"), self, lambda:1)
        hotkey('Alt+F2', self.toggle_hide)
        # window setu(p
        self.autosize()
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        # status bar
        self.statusBar().setSizeGripEnabled(True)
        self.statusBar().addWidget(QtWidgets.QLabel(f' PoEHelper {app.cfg.appinfo.ver_str} '))
        self.but.update = QtWidgets.QPushButton(' Update ')
        self.but.update.clicked.connect(self.update)
        self.statusBar().addWidget(self.but.update)
        # signals
        self.set_ui_text_sig.connect(self.set_ui_text_slot)
        self.toggle_hide_sig.connect(self.toggle_hide_slot)
        self.show_ref_panel_sig.connect(self.show_ref_panel_slot)
        rowheight = QtGui.QFontMetrics(self.eventlog.font()).lineSpacing()
        self.eventlog.setFixedHeight(10 + 3 * rowheight)
        self.eventlog.setOpenExternalLinks(True)
        self.eventlog.setOpenLinks(False)
        self.eventlog.setFocus()
        self.eventlog.anchorClicked.connect(self.eventlog_link_clicked)
        self.next_expand_size = None
        self.was_in_town_or_ho = False
        self.was_hidden = False
        self.reference.autosize()
        self.autosize()
        self.refdesc[-1].setFocus()
        self.plugin_panels = []


    def update(self):
        """
        Check for updates and, if available, install them.
        """
        self.but.update.setEnabled(False)
        delayed_run(self.app.update, timeout=0.2)


    def counter_clicked(self):
        """
        Increment the counter.
        """
        self.counter += 1
        self.but.counter.setText(str(self.counter))


    def counter_reset(self):
        """
        Reset the counter.
        """
        self.counter = 0
        self.but.counter.setText(str(self.counter))


    def timer_clicked(self):
        """
        Start/reset the timer.
        """
        if self.timer:
            log(f'Timer restarted after {format_seconds(self.timer.restart())}.')
        else:
            self.timer = StoppableTimer(callback=self.set_ui_text, cbkargs=('but.timer',))
            self.timer.start()
            dbg('Starting timer...')


    def load_resources(self):
        """
        Load app resources from files (fonts, CSS, etc.)
        """
        cfg = self.app.cfg
        self.font = load_font(f'{cfg.paths.rsrc}/fonts/Fontin-Regular.ttf')
        self.font.setPointSize(9)
        self.qtapp.setFont(self.font)
        css = open(f'{cfg.paths.rsrc}/poehelper.css').read()
        if os.path.isfile(cfg.paths.user_css_file):
            css += open(cfg.paths.user_css_file)
        self.setStyleSheet(css)


    def moveEvent(self, evt):
        """
        Keep track of position.
        """
        self.cfg.winpos[('max', 'min')[int(self.hidden)]] = self.pos().toTuple()
        delayed_run(self.app.save_cfg, timeout=1)


    def mousePressEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        self.drag_base_pos = evt.pos()


    def mouseReleaseEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        self.drag_base_pos = None


    def mouseMoveEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        if getattr(self, 'drag_base_pos', None) and evt.buttons() & Qt.LeftButton:
            diff = evt.pos() - self.drag_base_pos
            self.move(self.pos() + diff)


    def add_reference_panel(self, name, description=None, panels=None, cols=4, tagged=False,
            parser=None, reference=None, or_regex=None):
        """
        Add a panel to the reference group.
        """
        if tagged:
            panel = TaggedReferencePanel(name, description=description, panels=panels, cols=cols,
                    notify_on_resize=self.reference, parser=parser, reference=reference,
                    or_regex=or_regex)
        else:
            panel = ReferencePanel(name, description=description, panels=panels, cols=cols,
                    notify_on_resize=self.reference, parser=parser, reference=reference)
        self.reference.add_panel(name, panel)



    def show_reference_panel(self, panel, subpanels=None, show=True):
        """
        Show (open) a reference panel / (set of) subpanel(s).
        """
        self.show_ref_panel_sig.emit(panel, subpanels, show)


    @Slot(str, object, bool)
    def show_ref_panel_slot(self, panel, subpanels, show):
        """
        Show reference panel slot.
        """
        if not show:
            self.reference.panels[panel].hide_all_panels()
            self.reference.show_panel(panel, show=show)
            return
        self.reference.show_panel(panel, show=show)
        if subpanels:
            if type(subpanels) in (list, tuple):
                self.reference.panels[panel].show_panels(subpanels)
            else:
                self.reference.panels[panel].show_panel(subpanels, show=show)


    def reference_search(self):
        """
        Search references for text.
        """
        if not self.ref_search_timer:
            self.ref_search_timer = QTimer(self)
            self.ref_search_timer.timeout.connect(self._reference_search)
            self.ref_search_timer.setSingleShot(True)
        self.ref_search_timer.setInterval(500)
        if not self.ref_search_timer.isActive():
            self.ref_search_timer.start()


    def _reference_search(self):
        self.ref_search_timer = None
        text = self.refdesc[-1].text().lower().strip()
        if (not text) or (len(text) < 3):
            self.reference.hide_all_panels()
            self.open_ref_panels = []
            return
        for pname, panel in self.reference.panels.items():
            panel.find_text(text)


    def create_plugin_panel(self, title, text_ui):
        """
        Creates a plugin's panel based on the layout described in `text_ui`.
        """
        panel = PluginPanel(title, fields_as_str=text_ui)
        self.plugins.add_panel(title, panel)
        self.plugins.autosize()
        return panel


    def autosize(self):
        """
        Resize the window.
        """
        # self.setFixedSize(self.cfg.width, self.minimumSizeHint().height())
        # hack: wait for potential resize event to propagate to layouts
        for i in range(10):
            self.qtapp.processEvents()
        self.resize(self.cfg.width, self.sizeHint().height())


    def raise_app(self):
        """
        Bring the application into focus.
        """
        if self.hidden:
            self.toggle_hide()
        # self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        # self.show()
        return True


    def hide_app(self):
        """
        Hide the app.
        """
        if not self.hidden:
            self.toggle_hide()
        return True


    def toggle_hide(self):
        self.toggle_hide_sig.emit()


    @Slot()
    def toggle_hide_slot(self):
        """
        Hide or show the app.
        """
        hide = not self.hidden
        self.eventlog.setVisible(not hide)
        self.reference.setVisible(not hide)
        self.plugins.setVisible(not hide)
        self.statusBar().setVisible(not hide)
        if self.next_expand_size:
            height = self.next_expand_size
            self.next_expand_size = None
        else:
            height = self.cfg.height
        self.setMaximumHeight(40 if hide else 100000)
        self.hidden = hide
        self.autosize()
        new_pos = self.cfg.winpos[('max', 'min')[int(self.hidden)]]
        if new_pos != self.pos().toTuple():
            self.move(*new_pos)
        if hide:
            win.SetForegroundWindow(win.FindWindow(None, 'Path of Exile'))


    def close(self):
        """
        Close the GUI.
        """
        if self.timer:
            self.timer.stop = 1
        self.app.close()


    def run(self):
        """
        Start the GUI main loop.
        """
        self.qtapp.exec_()


    def set_ui_text(self, node, value, cat=None):
        """
        Allows setting UI elements from other threads (signal/slot mechanism).
        """
        if cat:
            node = f'{cat}.{node}'
        value = str(value)
        self.set_ui_text_sig.emit(node, value)


    @Slot(str, str)
    def set_ui_text_slot(self, node, value):
        """
        Slot for the .set_ui_text_sig() signal.
        """
        if '.' in node:
            cat, node = node.split('.', 1)
            obj = getattr(self, cat)[node]
            if cat in ('lbl', 'but'):
                obj.setText(value)
            elif cat == 'ind':
                obj.setIndState(int(value))
            else:
                raise ValueError(f'Unknown category [{cat}]!')
        elif node == 'eventlog':
            if self.cfg.log_timestamps:
                value = f"<font color='#432'>{time.strftime('%H:%M:%S')}</font> {value}"
            self.eventlog.append(value)
            self.eventlog.ensureCursorVisible()
        else:
            raise ValueError(f'Unknown UI component [{node}]!')


    def handle_event(self, etype, earg, **kwargs):
        """
        Handle app event.
        """
        if etype == 'area_changed':
            self.set_ui_text('lbl.area', f'{earg}')
            hide = None
            if (kwargs['in_town'] or kwargs['in_hideout']) and self.cfg.auto_show and not self.was_in_town_or_ho:
                hide = self.was_hidden
            elif not (kwargs['in_town'] or kwargs['in_hideout']) and self.cfg.auto_hide:
                self.was_hidden = self.hidden
                hide = True
            if hide is not None and hide != self.hidden:
                self.toggle_hide_sig.emit()
            if self.cfg.auto_collapse_reference:
                for panel in self.reference.panels.values():
                    panel.hide_all_panels()
            self.was_in_town_or_ho = kwargs['in_town'] or kwargs['in_hideout']
        elif etype in ('afk', 'game_running'):
            self.set_ui_text(f'ind.{etype}', int(earg))
        elif etype == 'pm':
            if not self.cfg.show_pms:
                return
            msg = (earg[:70] + '[...]') if len(earg) > 70 else earg
            msg = f'PM <font color=red>{escape(kwargs["sender"])}</font>: {escape(msg)}'
            self.set_ui_text('eventlog', msg)
        else:
            # ignore unknown events
            pass
            # msg = f'<font color=blue>{etype}</font>: <b>{earg}</b> ({kwargs})'
            # self.set_ui_text('eventlog', msg)


    def msg(self, msg):
        """
        Show message in event log.
        """
        if self.cfg.ignore_dbg_messages and msg.startswith('[#] '):
            return
        msg = re.sub(r'^\[([!#*])\] (.+)',
                lambda m:f'<font color="{LOG_COLOR_MAP[m[1]]}">{m[2]}</font>', msg)
        self.set_ui_text('eventlog', msg)


    def eventlog_link_clicked(self, url):
        if url.scheme() == 'ref':
            # open reference panel
            panel, subpanel = url.path().split('/', 1)
            self.show_reference_panel(panel, subpanel)
            return True



def create_ui(app):
    qtapp = QtWidgets.QApplication()
    qtapp.setApplicationName('PoE Helper')
    window = PoehMainWindow(app=app)
    window.show()
    return window


def load_font(font_file):
    """
    Load a font from a (TTF/OTF) file and return it.
    """
    font_file = font_file.replace('/', '\\')
    db = QtGui.QFontDatabase()
    font_id = db.addApplicationFont(font_file)
    if font_id == -1:
        err(f'Failed loading font Fontin from {font_file}!')
        return QtGui.QFont('Tahoma')
    name = db.applicationFontFamilies(font_id)[0]
    font = QtGui.QFont(name)
    dbg(f'Loaded font [{name}] from file [{font_file}]')
    return font

