#!/usr/bin/env python3
"""
PoEHelper - misc function library.

Author: Vlad Topan (vtopan/gmail)
"""

import threading
from tkinter import Tk, TclError  # Windows only, for easy clipboard access
import time
import traceback

from PIL import ImageGrab
import keyboard

from iolib.win import get_window_rect, FindWindow


DELAYED_RUNNERS = []


class adict(dict):
    """
    Attribute dict - contents are available through attributes as well as through keys.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self


def read_clipboard():
    """
    Get Windows clipboard contents.
    """
    try:
        text = Tk().clipboard_get()
    except TclError:     # ignore non-text clipboard contents
        text = None
    return text


def hotkey(key_name, handler, args=None, suppress=True, timeout=0):
    """
    Hooks a keyboard event ("hotkey").

    :param key_name: String describing the shortcut (e.g. 'ctrl+a').
    :param handler: Callback function handling the event, will receive the `arg` argument.
    :param arg: Argument passed to the callback.
    """
    keyboard.add_hotkey(key_name, handler, args=args) # , suppress=suppress)


def brief_exc(exc, html=True):
    """
    Return a brief text describing the last exception.
    """
    location = traceback.format_exc().strip().split('\n')[-3]
    location = location.split('\\')[-1].replace('", line ', ':')
    estring = f'{exc.__class__.__name__}: {exc} @ {location}'
    if html:
        estring = estring.replace('<', '&lt;').replace('>', '&gt;')
    return estring



def game_hwnd():
    """
    Return the HWND of the game window (or None).
    """
    return FindWindow(None, 'Path of Exile')


def game_screenshot():
    """
    Takes a screenshot of the game window.

    :return: A PIL image.
    """
    rect = get_window_rect('Path of Exile')
    img = ImageGrab.grab(tuple(rect))
    return img


def make_link(text, target):
    """
    Create a link to be displayed in a UI component.
    """
    return f'<a href="{target}" style="color:#C30;text-decoration:none;">{text}</a>'


def format_seconds(t):
    """
    Format a time in seconds as a "[H:]:MM:SS" string.
    """
    res = '%02d:%02d' % ((t // 60) % 60, t % 60)
    if t > 3600:
        res = f'{t//3600}:{res}'
    return res


def delayed_run(fun, args=None, timeout=5):
    """
    Run `fun` after timeout elapses (repeated calls reset the timer).

    :return: The runner object with .reset() and .cancel() methods.
    """
    for e in DELAYED_RUNNERS[:]:
        if e.completed:
            DELAYED_RUNNERS.remove(e)
        if e.callback == fun:
            e.reset()
            return e
    else:
        runner = DelayedRun(callback=fun, cbkargs=args, timeout=timeout)
        DELAYED_RUNNERS.append(runner)
        runner.start()
    return runner


def cancel_delayed_runners():
    """
    Cancel all delayed runners.
    """
    for e in DELAYED_RUNNERS:
        e.cancel()



class StoppableTimer(threading.Thread):


    def __init__(self, callback, cbkargs, *args, **kwargs):
        super().__init__()
        self.stop = 0
        self.elapsed = 0
        self.callback = callback
        self.cbkargs = cbkargs


    def run(self):
        self.start_time = time.time()
        while not self.stop:
            self.elapsed = int(time.time() - self.start_time)
            args = self.cbkargs + (format_seconds(self.elapsed),)
            self.callback(*args)
            time.sleep(0.5)


    def restart(self):
        """
        Restart timer and return elapsed time.
        """
        elapsed = self.elapsed
        self.start_time = time.time()
        return elapsed



class DelayedRun(threading.Thread):
    """
    Run a function after the timeout elapses; the timeout can be reset.
    """

    def __init__(self, callback, cbkargs=None, timeout=5, *args, **kwargs):
        super().__init__()
        self.callback = callback
        self.cbkargs = cbkargs or []
        self.timeout = self.left = timeout
        self.completed = False
        self.stop = False


    def reset(self, timeout=None):
        """
        Reset the timeout to the given value (default: its initial value).
        """
        self.left = timeout or self.timeout


    def cancel(self):
        """
        Cancel running the callback.
        """
        self.stop = 1


    def run(self):
        self.start_time = time.time()
        while not self.stop:
            if self.left <= 0:
                self.callback(*self.cbkargs)
                self.completed = 1
                break
            time.sleep(0.1)
            self.left -= 0.1


