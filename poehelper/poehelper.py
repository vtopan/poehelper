"""
PoEHelper: Path of Exile pluginable overlay helper.

Author: Vlad Topan (vtopan/gmail)
"""

import copy
import json
import os
import re
import shutil
import sys
import time
import zipfile

from .poeh_events import EventMgr
from .poeh_logging import log, err, dbg, set_log_callback, get_log_file, set_log_file, set_debugging
from .poeh_plugmgr import PlugMgr
from .poeh_ui import create_ui
from .poeh_utils import adict, cancel_delayed_runners, brief_exc

import iolib.net as net


PROJ_PATH = os.path.dirname(os.path.dirname(__file__))
if PROJ_PATH not in sys.path:
    sys.path += [PROJ_PATH, rf'{PROJ_PATH}\lib']
ROOT_URL = 'https://gitlab.com/vtopan/'
UPDATE_IGNORE = {
    'poehelper': re.compile(r'^(io|\.|req|\w+\.bat|run)',),
    'iolib': re.compile(r'^(\.|tools)',),
    'iopoelib': re.compile(r'^(\.|tests|tools)',),
    }


class PoEHelper():
    """
    Main program class.
    """


    def __init__(self):
        self.load_cfg(init=True)
        if get_log_file() == 'poehelper.log':
            set_log_file(rf'{self.cfg.paths.out}\poehelper.log')
        set_debugging(self.cfg.options.debug)
        if not os.path.isdir(self.cfg.paths.out):
            os.makedirs(self.cfg.paths.out)
        self.ui = create_ui(app=self)
        set_log_callback(self.ui.msg)
        self.plug_mgr = PlugMgr(app=self)
        self.evt_mgr = EventMgr(app=self)
        self.save_cfg()
        dbg(f'Program started @ {time.strftime("%H:%M:%S, %d.%m.%Y")}')


    def close(self):
        """
        Close the app.
        """
        self.evt_mgr.close()
        cancel_delayed_runners()
        dbg(f'Program closed @ {time.strftime("%H:%M:%S, %d.%m.%Y")}')
        sys.exit()


    def run(self):
        """
        Run the app.
        """
        self.ui.run()


    def save_cfg(self, app=True, plugins=True):
        """
        Save the configuration file.
        """
        if app:
            json.dump(self.cfg, open(self.cfg_file, 'w'), indent=2, sort_keys=1)
        if plugins:
            pcfg = copy.deepcopy(self.cfg_plugins)
            for p, v in pcfg.items():
                if 'game' in v:
                    del v['game']
                if 'paths' in v:
                    del v['paths']
            json.dump(pcfg, open(self.plug_cfg_file, 'w'), indent=2, sort_keys=1)


    def load_cfg(self, init=True):
        """
        Load the configuration file.
        """
        if init:
            cfg = self.cfg = adict()
            self.cfg_plugins = adict()
            paths = cfg.paths = adict(proj=os.path.dirname(os.path.dirname(__file__)))
            if paths.proj not in sys.path:
                sys.path.append(paths.proj)
            paths.data = rf'{paths.proj}\data'
            if not os.path.isdir(paths.data):
                paths.data = paths.proj
            paths.plugins = os.path.join(paths.data, 'plugins')
            paths.local = os.path.join(paths.proj, '.local')
            paths.local_plugins = os.path.join(paths.local, 'plugins')
            paths.out = os.path.join(paths.data, 'out')
            paths.doc = os.path.join(paths.data, 'doc')
            paths.rsrc = os.path.join(paths.data, 'rsrc')
            paths.user_css_file = os.path.join(paths.rsrc, 'user_style.css')
            paths.data_cache = os.path.join(paths.rsrc, 'data-cache')
            paths.game_docs = os.path.expandvars(r'%USERPROFILE%\Documents\My Games\Path of Exile')
            paths.screenshots = paths.game_docs + r'\Screenshots'
            cfg.ui = adict(width=400, height=200, ignore_dbg_messages=True, auto_hide=True,
                    auto_show=True, show_pms=True, auto_collapse_reference=False,
                    enable_timer=True, enable_counter=True, log_timestamps=False)
            cfg.options = adict(watch_client_txt=True, poll_frequency=0.1, log_tail=-1, debug=False)
            cfg.plugins = adict()
            cfg.game = adict(playername=None, league='Synthesis')
            if str(cfg.game.league).split(' ')[0].lower() == 'betrayal':
                league = 'Synthesis'
            self.cfg_file = f'{self.cfg.paths.data}/config.json'
            self.plug_cfg_file = f'{self.cfg.paths.data}/config_plugins.json'
        # read version
        ver_file = f'{cfg.paths.proj}\\version.txt'
        if not os.path.isfile(ver_file):
            err('Version file missing!')
        ver_type = 'binary' if os.path.isfile(f'{cfg.paths.proj}\\poehelper.exe') else 'source'
        ver_str = open(ver_file).read().strip()
        ver = [int(x) for x in ver_str.split('.')]
        cfg.appinfo = adict(ver=ver, ver_str=ver_str, ver_file=ver_file, binary=ver_type == 'binary')
        dbg(f'App version: {ver_str}')
        if os.path.isfile(self.plug_cfg_file):
            self.cfg_plugins = json.load(open(self.plug_cfg_file))
        if not os.path.isfile(self.cfg_file):
            if not init:
                raise ValueError(f'Missing configuration file [{self.cfg_file}]!')
            return self.cfg
        if not os.path.getsize(self.cfg_file):
            return self.cfg
        appinfo = self.cfg.pop('appinfo')
        for k, v in json.load(open(self.cfg_file)).items():
            if k not in self.cfg and k not in ('plugins',):
                self.cfg[k] = adict()
            for kk, vv in v.items():
                # only override fields present in the config file
                self.cfg[k][kk] = vv
        self.cfg.appinfo = appinfo
        return self.cfg


    def update(self):
        """
        Check for updates and update if possible.
        """
        tmp_files = []
        try:
            data = net.download(f'{ROOT_URL}poehelper/raw/master/version.txt', cache=0)
            ver_str = data.decode('utf8').strip()
            new_ver = [int(x) for x in ver_str.split('.')]
            if new_ver <= self.cfg.appinfo.ver:
                log(f'You have the latest version ({ver_str}).')
                self.ui.but.update.setEnabled(True)
                return
            log(f'Newer version is available ({ver_str}). Updating...')
            self.ui.but.update.setVisible(False)
            update_dir = f'{self.cfg.paths.proj}\\update'
            if not os.path.isdir(update_dir):
                os.makedirs(update_dir)
            for proj in ('poehelper', 'iolib', 'iopoelib'):
                zip_fn = f'{update_dir}\\{proj}-{ver_str}.zip'
                if not os.path.isfile(zip_fn):
                    log(f'Downloading {proj}...')
                    zip_data = net.download(f'{ROOT_URL}{proj}/-/archive/master/{proj}-master.zip',
                        cache=0)
                    open(zip_fn + '.tmp', 'wb').write(zip_data)
                    os.rename(zip_fn + '.tmp', zip_fn)
            for proj in ('poehelper', 'iolib', 'iopoelib'):
                zip_fn = f'{update_dir}\\{proj}-{ver_str}.zip'
                zf = zipfile.ZipFile(zip_fn)
                for zfn in zf.namelist():
                    relfn = zfn.replace('/', '\\').split('\\', 1)[1]
                    if not relfn or relfn.endswith('\\'):
                        continue
                    if UPDATE_IGNORE[proj].search(relfn):
                        continue
                    if proj != 'poehelper':
                        relfn = f'{proj}\\{relfn}'
                    relpath = os.path.dirname(relfn)
                    if relpath and not os.path.isdir(relpath):
                        os.makedirs(relpath)
                    open(relfn + '.update', 'wb').write(zf.read(zfn))
                    tmp_files.append(relfn)
                zf.close()
            log('Updating files...')
            for f in tmp_files:
                if f.endswith('.ttf'):
                    shutil.move(f, f + '.deleteme')
                shutil.move(f + '.update', f)
            for proj in ('poehelper', 'iolib', 'iopoelib'):
                zip_fn = f'{update_dir}\\{proj}-{ver_str}.zip'
                os.remove(zip_fn)
            log('Updated; please restart PoEHelper.')
        except Exception as exc:
            for f in tmp_files:
                f = f + '.update'
                if os.path.isfile(f):
                    try:
                        os.remove(f)
                    except:     # NOQA
                        pass
            err(f'Update failed: {brief_exc(exc)}!')

