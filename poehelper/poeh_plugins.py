"""
PoEHelper plugin template + plugin API implementation.

Author: Vlad Topan (vtopan/gmail)
"""

import glob
import inspect
import os
import re
import threading
import time

import keyboard
from PIL import ImageGrab
# import numpy as np

import iopoelib.net as poenet

from .poeh_constants import get_trade_mapping, get_image_path
from .poeh_utils import adict, brief_exc, game_screenshot, game_hwnd, make_link as pu_make_link, delayed_run
from .poeh_logging import log, err, dbg


IMAGE_CACHE = {}
MD_COLORS = {
    '+': '#0A0',
    '-': '#A00',
}
MD_HIGHLIGHT = {
    1: '<font color=%s>%s</font>',
    2: '<b><font color=%s>%s</font></b>',
}
MD_FMT = {
    '*': 'i',
    '**': 'b',
    '__': 'u',
}

# used by the send_macro API
CHAR_MAP = {
    'cr': 'enter',
    }



class PoehPlugin:
    """
    Template plugin class.

    Each plugin can have a section with its own name in `config.json`; it will be accessible through
    `self.cfg` at runtime and the default values must be set in the plugin class variable `CFG` (a
    dict).
    """

    def __init__(self, cfg, paths, api):
        self.cfg = cfg
        self.paths = paths
        self.api = api
        if hasattr(self, 'init'):
            try:
                self.init()
            except Exception as e:
                name = self.__class__.__name__
                err(f'Plugin {name} failed custom init: {brief_exc(e)}!')
                raise



def caller():
    stack = inspect.stack()
    cclass = stack[2][0].f_locals['self']
    return cclass


def caller_class():
    stack = inspect.stack()
    cclass = stack[2][0].f_locals['self'].__class__.__name__
    return cclass



class PlugApi:
    """
    APIs available to plugins.
    """

    def __init__(self, mgr):
        self._mgr = mgr
        self._paths = mgr.app.cfg.paths
        self.log = log
        self.err = lambda msg:err(f'{caller_class()}: {msg}')
        self.dbg = lambda msg:dbg(f'{caller_class()}: {msg}')
        self.poecom = adict()
        self.poeninja = adict()
        for k, v in poenet.__dict__.items():
            if k.startswith('poecom_'):
                self.poecom[k.split('_', 1)[1]] = v
            elif k.startswith('poeninja_'):
                self.poeninja[k.split('_', 1)[1]] = v


    @property
    def _plugin(self):
        """
        API caller plugin.
        """
        stack = inspect.stack()
        plugin = stack[2][0].f_locals['self']
        return plugin


    @property
    def in_hideout(self):
        """
        Returns True if the player is currenty in a hideout.
        """
        return self._mgr.app.evt_mgr.state.in_hideout


    @property
    def in_town(self):
        """
        Returns True if the player is currenty in a town.
        """
        return self._mgr.app.evt_mgr.state.in_town


    def add_game_hotkey(self, key, callback, args=None):
        """
        Add a hotkey to the game.

        :param key: Described as a string (e.g. "Ctrl+X").
        :param callback: Function to call.
        :param args: Args to pass to callback.
        """
        self._mgr.add_game_hotkey(key, callback, args=args)


    def send_macro_callback(self, text):
        """
        Use this to send a macro to the currently active window.

        :param text: Text to send; encode special characters between curly braces (e.g. "{esc}{cr}")
        """
        def _inner():
            for e in re.findall(r'\{[^\}]+\}|[^\{\}]+', text):
                if e[0] == '{':
                    e = e[1:-1]
                    if e.isnumeric():
                        e = int(e)
                    else:
                        e = CHAR_MAP.get(e.lower(), e)
                    keyboard.send(e)
                else:
                    keyboard.write(e)
        return _inner


    def trade_short_name(self, item):
        """
        Translate a currency/scarab/fragment name into the short name used by the official trade site.
        """
        cmap = get_trade_mapping(cache_path=self._paths.data_cache)
        return cmap.get(item, [None])[0]


    def trade_full_name(self, item):
        """
        Translate a currency/scarab/fragment short name into the full name.
        """
        cmap = get_trade_mapping(cache_path=self._paths.data_cache, inverted=True)
        return cmap.get(item)


    def trade_category(self, item):
        """
        Find the category of an item (currency/scarab/fragment).
        """
        cmap = get_trade_mapping(cache_path=self._paths.data_cache)
        if item in cmap:
            return cmap[item][2]
        imap = get_trade_mapping(cache_path=self._paths.data_cache, inverted=True)
        if item in imap:
            return cmap[imap[item]][2]
        return None


    def guess_type_from_name(self, itemname):
        """
        Try to guess the item type from the name.
        """
        if itemname.endswith('Scarab'):
            itemtype = 'scarab'
        elif itemname.split(' ', 1)[0] in ('Mortal', 'Fragment', 'Sacrifice'):
            itemtype = 'fragment'
        else:
            itemtype = None
        return itemtype


    def get_image_string(self, itemtype, itemname, width=16, height=16):
        """
        Get the image resource formatted to be inserted in the event log.

        :param itemtype: The base item type ('item', 'gem', 'currency', etc.).
        :param itemname: The item's name (e.g. "Vaal Orb").
        """
        if itemtype is None:
            itemtype = self.guess_type_from_name(itemname) or 'trade'
        try:
            ipath = get_image_path(self._paths.data_cache, itemtype, itemname)
        except ValueError as e:
            err(str(e))
            return None
        if ipath:
            ipath = ipath.replace('\\', '/')
            ipath = ipath.replace(self._paths.proj.replace('\\', '/') + '/', '')
            return f'<img height={height} width={width} src="%s" />' % \
                ipath.replace("\\", "/")
        else:
            return None


    def take_game_screenshot(self, as_file=False, wait=0.5, via_f8=False):
        """
        Takes a screenshot in-game and returns the PIL Image or file name.
        """
        screens = glob.glob(rf'{self._paths.screenshots}\screenshot-????.png')
        if via_f8:
            self.send_macro_callback('{f8}')()
            time.sleep(wait)
        else:
            if as_file:
                last_id = int(screens[-1].rsplit('-', 1)[1].split('.')[0]) if screens else 0
                fn = rf'{self._paths.screenshots}\screenshot-{last_id+1:04d}.png'
            img = game_screenshot()
            if not as_file:
                return img
            img.save(fn)
            return fn
        new_screens = glob.glob(rf'{self._paths.screenshots}\screenshot-????.png')
        if (not new_screens) or len(screens) == len(new_screens):
            self.err('Failed taking screenshot!')
            return
        return new_screens[-1]


    def take_screenshot(self, rect=None):   # , as_cv2=None):
        """
        Takes a screenshot of the entire screen (or a region) and returns the PIL Image.

        :param rect: If given, must be a sequence of 4 integers: (left, top, right, bottom).
        """
        # :param as_cv2: Convert image to cv2 format (numpy BGR 3D array).
        img = ImageGrab.grab(rect)
        # if as_cv2:
        #    img = np.array(img.convert('RGB'))[:, :, ::-1].copy()
        return img


    def get_game_hwnd(self):
        """
        Returns the HWND of the game window (or None).
        """
        return game_hwnd()


    def temporarily_expand(self):
        """
        Temporarily expand the main window.
        """
        self._mgr.app.ui.next_expand_size = 500
        if self._mgr.app.ui.hidden:
            self._mgr.app.ui.toggle_hide()


    def add_reference_panel(self, name, description=None, panels=None, cols=4, tagged=False,
            reference=None, or_regex=None):
        """
        Add a reference category (description is always on, panels are toggle-able).

        :param description: The contents of the first (always-on) panel.
        :param panels: A dict-like mapping of categories to text.
        """
        self._mgr.app.ui.add_reference_panel(name, description, panels, cols=cols, tagged=tagged,
                parser=self.md_to_html, reference=reference, or_regex=or_regex)


    def show_reference_panel(self, panel, subpanels=None, show=True):
        """
        Show/hide a reference category/panel (and optionally subpanel(s)).
        """
        self._mgr.app.ui.show_reference_panel(panel, subpanels=subpanels, show=show)


    def create_plugin_panel(self, title, text_ui):
        """
        Creates the plugin's panel based on the layout described in `text_ui`.
        """
        self._plugin.panel = self._mgr.app.ui.create_plugin_panel(title, text_ui)


    def show_plugin_panel(self, show=True):
        """
        Show the plugin's panel.
        """
        self._mgr.app.ui.plugins.show_panel(caller().panel.title(), show=show)


    def run_threaded(self, fun, args=None):
        """
        Run the given function in a separate thread.
        """
        def _thread_runner(f, args):
            def _wrapper():
                try:
                    f(*(args or []))
                except Exception as e:
                    err(f'Plugin thread {fun.__name__}() crashed: {brief_exc(e)}!')
            return _wrapper
        threading.Thread(target=_thread_runner(fun, args)).start()


    def new_event(self, etype, earg=None, **kwargs):
        """
        Creates (and sends) an event (to other plugins).

        :param etype: The event type (string).
        :param earg: The event argument (can have any type); additional arguments go in
            additional named parameters.
        """
        self._mgr.app.evt_mgr.new_event(etype, earg, source=caller_class(), **kwargs)


    def make_link(self, text, target):
        """
        Creates an <a> tag to be displayed in a UI component.
        """
        return pu_make_link(text, target)


    def wiki_url(self, name):
        """
        Returns the wiki URL for the given name.
        """
        return poenet.wiki_url(name)


    def make_evtlog_ref_url(self, category, name):
        """
        Returns the local eventlog-compatible URL pointing to the given reference item.
        """
        return f'ref:{category}/{name}'


    def md_to_html(self, text):
        """
        Convert custom markdown to HTML.
        """
        text = re.sub(r'^\|(?:(?:.+?\|){2,}(?:\s*\r?\n|$)){2,}', lambda m:'<table class=reftable>%s</table>' % \
                ''.join('<tr>%s</tr>' % ''.join(f'<td>{ee.strip()}</td>' for ee in e.split('|')) \
                for e in m[0].split('\n')), text, flags=re.M)
        text = re.sub(r' (https?://(.+?))(?=\s|$)', lambda m:' ' + self.make_link(m[2], m[1]), text,
                flags=re.M)
        text = re.sub(r'(\*\*|\*|__)(\S.*?\S)(\1)',
                lambda m:f'<{MD_FMT[m[1]]}>{m[2]}</{MD_FMT[m[1]]}>', text)
        text = re.sub(r'\[([\w \'-]+)\]', lambda m:self.make_link(m[1], self.wiki_url(m[1])), text)
        text = re.sub(r'(\{+)([ \w]+)\}+', lambda m:self.find_image(m[2]) + (len(m[1]) - 1) * (' ' + m[2]), text)
        text = re.sub(r'^([-+]{1,2})([^ ].+)',
                lambda m:MD_HIGHLIGHT[len(m[1])] % (MD_COLORS[m[1][0]], m[2]), text, flags=re.M)
        text = re.sub(r'^[ ]+', lambda m:'&nbsp;' * len(m[0]), text, flags=re.M)
        text = re.sub('^([^#\n][^\n]+)(?=\n)', r'\1<br>', text, flags=re.M)
        return text


    def find_image(self, name):
        """
        Find item image.
        """
        if name in IMAGE_CACHE:
            return IMAGE_CACHE[name]
        path = f'data/rsrc/images/{name.lower()}.png'
        if os.path.isfile(path):
            img = f'<img src="{path}" title="{name}" width=16 height=16>'
        else:
            fn = self.trade_full_name(name) or name
            cat = self.trade_category(fn)
            img = self.get_image_string(cat, fn, width=16, height=16)
            if not img:
                return '{%s}' % name
        IMAGE_CACHE[name] = img
        return img


    def delayed_run(self, fun, args=None, timeout=5):
        """
        Run `fun` after timeout elapses (repeated calls reset the timer).

        :return: The runner object with .reset() and .cancel() methods.
        """
        return delayed_run(fun, args=args, timeout=timeout)


    def save_plugin_configs(self):
        """
        Save the plugin configurations (for all plugins).
        """
        self._mgr.app.save_cfg(app=False, plugins=True)


    def brief_exc(self, exc):
        """
        Return the exception as a brief string.
        """
        return brief_exc(exc)

